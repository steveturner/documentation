---
layout: default
title: Seeker Pre-Flight Setup
parent: Seeker User Guide
nav_order: 35
has_children: false
permalink: /seeker-user-guide-preflight/
---

# Seeker Pre-Flight Setup
{: .no_toc }


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

{: .alert .danger-alert}
**WARNING:** For Safety <b> Disconnect the battery </b> before installing the props

### Install Propellers

Install the propellers following the orientation shown. Screw the nuts in by hand, then use an 8mm wrench to tighten them. Make sure to hold onto the motors, and not the props while tightening.

![seeker-props.png](/images/seeker/seeker-props.jpg)

The propellors are designed to turn inwards when facing the drone. When the propellors are facing you, the blades should **slope downwards** towards you. 
If your props have lettering on them, make sure it faces up. **Make sure your props are orientated like below.**

{: .alert .danger-alert}
**WARNING:**  Make Sure the Props are on Correctly!

![seeker-props-3.png](/images/seeker/seeker-blades.gif)

### Safety Check
Before flying, make sure to conduct a thorough safety check: 
<ul>
<li>Make sure all connectors and cabling is secure and that they are not loose </li>
<li>Make sure none of the wires are cut, frayed, or damaged </li>
<li>If you have an LTE version, make sure the antennas are secure and are not loose </li>
<li>Make sure the SD and SIM cards have not popped out </li>
<li>Make sure the reciever is properly plugged in </li>
<li>Make sure the cameras are free of any grease, dust, or debris </li>
<li>Make sure the battery you are using is properly charged </li>
<li>Make sure the props are on correctly and that they are securly attached </li>
</ul>
---

[Next Step: First Flight](/seeker-user-guide-firstflight/){: .btn .btn-green }
