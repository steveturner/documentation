---
layout: default
title: Sentinel Quickstart
parent: Sentinel
nav_order: 1
has_children: False
youtubeId: hMhQgWPLGXo
permalink: /sentinel-quickstart/
---

# Sentinel Reference Drone Quickstart
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Overview

The [User's Guide](/sentinel-user-guides/) walks through taking Sentinel out of the box and up into the air!

For technical details, see the [datasheet](/sentinel-datasheet/).

<br>
{% include youtubePlayer.html id=page.youtubeId %}


{: .alert .simple-alert}
**WARNING:** *Unmanned Aerial Systems (drones) are sophisticated, and can be dangerous.  Please use caution when using this or any other drone.  Do not fly in close proximity to people and wear proper eye protection.  Obey local laws and regulations.*

---

### Required Materials

To follow this user guide, you'll need the following:

- Spektrum Transmitter 
  - e.g. SPMR6655, SPM8000 or SPMR8000
  - Any Spektrum transmitter with DSMX/DSM2 compatibility will likely work
  - Buy [Here](https://www.modalai.com/products/m500-spektrum-dx6e-pairing-and-installation?_pos=2&_sid=8af1d1b9b&_ss=r) 
- Wall Power Supply or Battery with XT60 connector
  - e.g. Gens Ace 5000mAh, or any 3S battery with XT60 connector
  - Buy Battery [Here](https://www.modalai.com/products/gens-ace-11-1v-60c-3s-5000mah-lipo-battery-pack-with-xt60-plug?_pos=2&_sid=0558e5a83&_ss=r) 
  - Buy Wall Power Supply [Here](https://www.modalai.com/products/ps-xt60?_pos=1&_sid=f5b241f03&_ss=r)  
- Host computer with:
  - QGroundControl 3.5.6+
  - Ubuntu 18.04+
  - Wi-Fi
- USB Type C cable


