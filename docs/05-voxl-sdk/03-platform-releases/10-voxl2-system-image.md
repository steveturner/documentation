---
layout: default
title: VOXL 2 System Image
parent: Software Releases
nav_order: 10
permalink:  /voxl2-system-image/
youtubeId1: IM3PnW7cipQ
youtubeId2: W0b9CxMQOJk
---

# VOXL 2 (QRB5165) System Image
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Summary

### What is it?

The system image is essentially "the operating system", and consists of the kernel, root file system, and various firmwares for the plentiful amount of processors on board to name a few items.

### Do I need to install it?

If you have a `VOXL 2`, it comes pre-installed so there's nothing to do unless you are looking to install a newer version.

If you are having problems with your system, **please** ask questions on the [forum](https://forum.modalai.com/) before reflashing the system image.


### Download Location

You can [download the system image](https://developer.modalai.com/asset) from our protected downloads page. Although this is usually not necessary since the system image is bundled in our [Platform Releases](/platform-releases/).




## USB Cables

It is recommended to use a USBC to USBA cable.  

We have seen issues with USBC to USBC cables on some host machines.

## Backup Files

- Please backup the following manually prior to updating

```bash
❯ adb pull /etc/modalai .
❯ adb pull /data/misc .
```

{: .alert .danger-alert}
**WARNING:** ALL DATA IS LOST IN THE CURRENT SYSTEM IMAGE INSTALLATION

## Upgrade using Fastboot

### Overview

Android has tools used for flashing phones with new releases.  We can use the same tools here to flash the system image onto VOXL2.

### Video

{% include youtubePlayer.html id=page.youtubeId2 %}

### Procedure

- Unzip the download, in this example we'll assume the download name was `M.m.b-M0054-14.1a-perf.tar.gz` where M.m.b is the version.

```bash
❯ tar -xzvf M.m.b-M0054-14.1a-perf.tar.gz
```

- Get ready to run the script by going into the directory you just unzipped

```bash
❯ cd M.m.b-M0054-14.1a-perf
```

- Now, attach the VOXL 2 via USBC and ensure the unit is powered on
- Make sure that adb see's your by running `adb devices` from the host computer

```bash
❯ adb devices
List of devices attached
f8bb8d44	device
```

  **If no devices show up:** see `Force VOXL 2 into fastboot` below

- Check that the `fastboot` command works by running `fastboot devices`

```bash
❯ fastboot devices
```

**If command fails** see `Missing ADB?` below

- Run the following:

```bash
sudo ./flash-full.sh
```

Here's an example output:

```bash
❯ ./flash-full.sh
[INFO] version: 0.0
[INFO] adb installed
[INFO] fastboot installed
[INFO] qti-ubuntu-robotics-image-qrb5165-rb5-boot.img present
[INFO] abl.elf present
[INFO] qti-ubuntu-robotics-image-qrb5165-rb5-sysfs.ext4 present
[INFO] Rebooting to fastboot bootloader
[INFO] Flashing boot.img
< waiting for any device >
Sending 'boot_a' (20884 KB)                        OKAY [  0.055s]
Writing 'boot_a'                                   OKAY [  0.026s]
Finished. Total time: 0.131s
Sending 'boot_b' (20884 KB)                        OKAY [  0.062s]
Writing 'boot_b'                                   OKAY [  0.027s]
Finished. Total time: 0.133s
[INFO] Flashing abl.elf
Sending 'abl_a' (148 KB)                           OKAY [  0.003s]
Writing 'abl_a'                                    OKAY [  0.001s]
Finished. Total time: 0.012s
Sending 'abl_b' (148 KB)                           OKAY [  0.004s]
Writing 'abl_b'                                    OKAY [  0.001s]
Finished. Total time: 0.013s
[INFO] Flashing sysfs.ext4
Sending sparse 'system' 1/5 (784305 KB)            OKAY [  7.592s]
Writing 'system'                                   OKAY [  0.000s]
Sending sparse 'system' 2/5 (786429 KB)            OKAY [ 13.012s]
Writing 'system'                                   OKAY [  0.000s]
Sending sparse 'system' 3/5 (752290 KB)            OKAY [ 13.922s]
Writing 'system'                                   OKAY [  0.000s]
Sending sparse 'system' 4/5 (785699 KB)            OKAY [ 13.732s]
Writing 'system'                                   OKAY [  0.002s]
Sending sparse 'system' 5/5 (541781 KB)            OKAY [  9.620s]
Writing 'system'                                   OKAY [  0.001s]
Finished. Total time: 64.082s
[INFO] Rebooting...
Rebooting                                          OKAY [  7.045s]
Finished. Total time: 7.045s
[INFO] Waiting for device...
[INFO] Device ready, version:
1.1.2-M0054-14.1a-perf
[INFO] Finished
```

After this, you should be able to adb back into the device.  If needed, restore the files that you manually backed up.

## Force VOXL 2 into Fastboot

### Overview

This procedure is for use if the normal update procedure above is not working.

### Video

{% include youtubePlayer.html id=page.youtubeId1 %}

### Procedure

- Unplug VOXL 2 from power
- Unplug VOXL 2 from USBC
- Using something soft like a BBQ skewer or toothpick, press and hold the momentary button `SW1` down, as shown in this image:

<img src="/images/voxl2/m0054-fastboot.jpg" alt="m0054-fastboot" width="640"/>

- While holding `SW1` down, power on VOXL 2
- Keep holding `SW1` down for about 5 seconds and then let go
- Attach VOXL 2 to USBC connected to host computer
- Run the `fastboot devices` command and verify the device is showing up

```bash
❯ fastboot devices
f8bb8d44	fastboot
```

- Now, you should be able to proceed with the `./full-flash.sh` command

## Missing ADB?

Install the Android Debug Bridge (ADB):

```bash
me@mylaptop:~$ sudo apt install android-tools-adb android-tools-fastboot
```

*ModalAI Top Tip*: to run ADB without root on the host PC, create a file called `/etc/udev/rules.d/51-android.rules` containing this udev rule on the host computer:

```bash
me@mylaptop:~$ sudo su
root@mylaptop:/home/me# echo 'SUBSYSTEM=="usb", ATTRS{idVendor}=="05c6", ATTRS{idProduct}=="901d", MODE="0660", GROUP="plugdev", SYMLINK+="voxl%n"' > /etc/udev/rules.d/51-android.rules
root@mylaptop:/home/me# echo 'SUBSYSTEM=="usb", ATTRS{idVendor}=="18d1", ATTRS{idProduct}=="d00d", MODE="0660", GROUP="plugdev", SYMLINK+="voxl-fastboot%n"' >> /etc/udev/rules.d/51-android.rules
root@mylaptop:/home/me# udevadm control --reload-rules && udevadm trigger
```

## CHANGELOG

### 1.5.5 (Platform Release 0.9.5)

SDK Version: 0.9.5
Release Date: TBD
Build Date: 2023-03-22

Camera
- update IMX678 config:
  - 3840x2160@30 - use 4 lane mode at 891 Mbps
  - 1920x1080@30 - use 4 lane mode at 720 Mbps
  - enable on HW ID 3 to match other hires sensors
- revert defaultFpsMax backend flag (use case selection is broken when enabled)

### 1.5.4 (internal)

Build Date: 2023-02-16

HLOS
- Increase SLPI memory
- Install `voxl-esc` python depends (pip3,pyserial,numpy)
- add nano

NHLOS
- ModalAI SLPI upgraded to 1.1-6
- Increase SLPI memory
- Added 420k baud rate UART for SLPI (native ELRS baud rate)

### 1.5.3 (internal)

Build Date: 2023-02-03

BSP (See [Linux User Guide](/voxl2-linux-user-guide/) for details)
- VOXL2 (M0054)
  - configure GPIO 152 as output, default high, on J5 pin 44 for M0130 (along with GPIO 153, 154, 155 while at it)
  - configure SE1 for I2C `/dev/i2c-4` J7 (camera group 1)
  - enable new GPIO 110/114 pins on J6
  - enable new GPIO 6/7 pins on J7
  - enable new GPIO 12/13 pins on J8
  - enable SE2 (I2C) on J5 pins 8,9 as `/dev/i2c-0`
  - enable SE11 (SPI) on J5 pin 53,56 as `/dev/spidev11.0`
  - update TZ to devcfg ver8
    - see [voxl2-qups](/voxl2-qups/) for details

Camera
- Added IMX678 Support (M0054-J8, HW sensor ID 4/5 (addresses 0x34/0x20))
  - 1920 x 1080 @ 30 FPS
  - 3840 x 2160 @ 30 FPS
- ov7251 60/90/120 FPS configs added as dormant (need to add camx propper support)

HLOS
- Added service file that changes SLPI restart level to avoid board crash when SLPI crashes

NHLOS
- ModalAI SLPI upgraded to 1.1-4

### 1.4.1 (Platform Release 0.9)

Date: 2022-11-11

Ubuntu:
- add `i2c-tools`
- Journalctl system max use limit now set to 1000M

Camera:
- Add A65 TOF Module (irs10x0c sensor) support on all 6 camera interfaces
- ov7251 drivers are now 8-bit

XBL (secondary bootloader):
- Fix occasional error showing "battery's capacity is very low" during flashing

NHLOS:
- ModalAI SLPI upgraded to 1.1-2

Flashing Script:
- Made system image flash script compatible with bash 4 and older versions

### 1.3.1 Date: 

Date: 2022-08-11

Meta:
- Included missing fastrpc binaries for adsp and cdsp in dspso partition

Camera:
- Update IMX214 drivers to support other resolutions other than 640x480
- Added ALPHA LEVEL support for 5 concurrent ov9782 (needs tuning)

BSP
- M0054/M0052 - enable 2W UART on J3 pins 3/5 (legacy B2B) for apps proc usage
- M0052 - breaking change: change qup5 from SPI to UART.  This is exposed on J8.  Now /dev/spidev5.0 is gone.
- M0054 Mappings are as follows:
  - `/dev/ttyHS0`  - J8 Camera Connector
  - `/dev/ttyHS1`  - J3 B2B pins 3/5
  - `/dev/ttyHS2`  - J5 HS B2B pins 48/49
- M0052 Mappings are as follows:
  - `/dev/ttyHS0`  - internal SOM WiFi
  - `/dev/ttyHS1`  - J2 RC input for PX4
  - `/dev/ttyHS2`  - J19 GNSS input for PX4
  - `/dev/ttyHS3`  - J8 Camera Connector
  - `/dev/ttyHS4`  - J3 B2B pins 3/5
  - `/dev/ttyHS5`  - J5 HS B2B pins 48/49

Trustzone:
- M0054, version 6: update devcfg for UART on qup7 (md5sum: 3698389194c899953c4e337a7b48cb97)
- M0052, version 6: update devcfg for UART on qup7 (md5sum: cf613de37db6e7d2bb245f4d17cab79e)

### 1.2.3

(Internal release only)

Ubuntu:
- added jq pciutils aircrack-ng packages

Kernel:
- add dormant 88XXau_wfb.ko driver at /etc/  Users who choose to use it can install it manually and use voxl-wifi-broadcast 
- regulatory domain tweaks to support wifibroadcast options
- Update spidev buffer size to 16K

### 1.2.2

(Internal release only)

Changes:
- System:
  - Bash environment executed during ssh session start
  - ModalAI partitions now include MACHINE type prefix
  - ModalAI SLPI 1.1-0 now flashed in system image via DSP and Firmware partitions
- Kernel
  - fix for hal3 timestamp drift error
  - enable LAN95XX driver
- chi-cdk
  - ov9782: add front/rear and tracking support
  - ov7251: fix for missing gain in hal3 metadata

### 1.2.1

Date: 2022-04-18

Changes:
- ROS sources and key included in root filesystem
- XBL now flashed during system image install
- Binaries can now be executed in the data partition

### 1.2.0

Date: 2022-04-15

Changes:
- Update chi-cdk - ov7251 FSYNC registers from mm-camera
- Add rndis host driver to kernel
- Enable SPI
  - /dev/spidev3.0 - internal IMU
  - /dev/spidev14.0 - external SPI
  - /dev/spidev0.0, /dev/spidev1.0, and /dev/spidev5.0 - camera groups 0, 1 and 2
- Updated device’s HLOS partitions
  - Updated userdata ( /data ) partition to 64GB
  - Added 64KB modalai_conf ( /etc/modalai ) partition
  - Added 64KB modalai_cal ( /data/modalai ) partition
  - Reduced system ( / ) partition to 47.875GB
- System image flash
  - devcfg.mbn is now flashed during system image flash
  - NHLOS is now flashed during system image flash
  - LUN0 GPT can be flashed during system image flash
- System image root filesystem additions
  - Px4-support and libfc now included in rootfs
  - ModalAI metabuild info now in /firmware/verinfo/ver_info.txt
  - Remove /etc/voxl-platform-info.json meta file creation, ModalAI metabuild info includes this information now
  - Removed old incompatible tdk binaries
  - Added Stable ModalAI sources list, library search paths, and qrb5165-bind to root filesystem
  - Included docker sources list and key

### 1.1.4

Date: 2022-03-09

Changes:
- add rtl8188eus driver for tp-link support
- remove all opencv 3.2 packages
- apt autoremove and clean removing 400MB! of cached debs
- permit root ssh login
- add platform_name file for bash formatting
- remove ldconfig crying about wayland sink not a symlink

### 1.1.3

Date: 2022-02-18

Changes:
- Modified mv-voxl 32-bit shared object files permissions as they were set incorrectly
- Added ```/etc/voxl-platform-info.json``` file which includes build and platform metadata

### 1.1.2

Date: 2022-02-09

Changes:
- Initial Release

[Next: VOXL 2 Linux Guide](/voxl2-linux-user-guide/){: .btn .btn-green }
