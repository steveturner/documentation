---
layout: default
title: Improving VOXL Mapper's performance
parent: Mapping & Path Planning
nav_order: 10
permalink: /improving-performance/
---

# Improving VOXL Mapper's performance
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Improving Mapping
### Parameter tweaks
The mapping performance heavily depends on the set parameters. Below is a quick description of a few key parameters that help to speed up mapping and reduce its computational load. These parameters can be found in the mapper config file `/etc/modalai/voxl-mapper.conf`

`voxel_size`: Side length of a voxel within the map (in metres). Reducing this number will give higher resolution maps but will greatly increase computational load.

`esdf_max_distance`: The maximum distance to which the Euclidean Signed Distance Field (ESDF) will be calculated (in metres). The ESDF is a 3D map that returns the distance to the closest obstacle for any position within it. The ESDF is mainly used for planning. If you are only using VOXL mapper for mapping you can set this parameter to a very small number.


## Improving Planning
### Parameter tweaks
The planning performance can be improved by altering a few parameters. Below is a quick description of the parameters that will have the greatest effect.

`voxel_size`: Side length of a voxel within the map (in metres). If this number is very small the local planning will be slower as it has to search through more voxels.

`rrt_min_distance`: The length of an edge in the RRT Tree used in the planner. A larger number allows the planner to search faster but will limit its ability to squeeze through smaller gaps.

**Important Note**: When using the planning capabilities it is important to ensure that the following parameters are set correctly:

`robot_radius`: Radius of your robot (in metres)

`esdf_max_distance`: The maximum distance to which the Euclidean Signed Distance Field (ESDF) will be calculated (in metres). This should be set to a value larger than the robot radius.

`esdf_default_distance`: For unobserved voxels or voxels further from obstacles than `esdf_max_distance` this is the value that the ESDF will return. A good rule of thumb is to set this to the same value as `esdf_max_distance`.
