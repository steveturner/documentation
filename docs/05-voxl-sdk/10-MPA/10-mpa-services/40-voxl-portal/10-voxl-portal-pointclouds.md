---
layout: default
title: Viewing Pointclouds
parent: VOXL Portal
nav_order: 10
has_children: false
permalink: /voxl-portal-pointclouds/
youtubeId:
---

# Viewing Pointclouds
{: .no_toc }

voxl-portal uses the MPA library to dynamically list and display any open pointcloud pipes.
When the "Pointclouds" dropdown is selected, any available pointcloud output will be displayed. All MPA pointcloud types are supported, see [libmodal-pipe](https://gitlab.com/voxl-public/modal-pipe-architecture/libmodal_pipe/-/blob/master/library/include/modal_pipe_interfaces.h#L434) for the full list.

![voxl-portal-pointcloud-list.png](/images/voxl-sdk/voxl-portal/voxl-portal-pointcloud-list.png)

Select the pointcloud you wish to view, and you will taken to the pointcloud page where your cloud of points is displayed over a grid. The pointcloud viewer utilizes WebGl for gpu accelerated rendering, so running on devices that do not support this may result in other issues.

![voxl-portal-dfs-pointcloud.png](/images/voxl-sdk/voxl-portal/voxl-portal-dfs-pointcloud.png)


The bottom left hand corner of the page includes a button to overlay an image stream on top of the pointcloud grid. Clicking on this tv icon will populate a list above of all available image topics, similar to the camera dropdown. 

![voxl-portal-overlay-stream.png](/images/voxl-sdk/voxl-portal/voxl-portal-overlay-stream.png)

Selecting one will overlay it on the page, where it can be resized via the bottom left corner of the stream and closed using the red x at the bottom left. 

![voxl-portal-overlay-stream-qvio.png](/images/voxl-sdk/voxl-portal/voxl-portal-overlay-stream-qvio.png)

The bottom right corner of the page will show a 3-axis view of your current camera angle to help with orientation. 

![voxl-portal-axes.png](/images/voxl-sdk/voxl-portal/voxl-portal-axes.png)

The top right corner of the page (within the header) will also include a black/white slider to switch between light and dark modes.

![voxl-portal-dark-mode.png](/images/voxl-sdk/voxl-portal/voxl-portal-dark-mode.png)

## Tof
voxl-portal has a special viewer for the "Tof" pointcloud topic if your VOXL setup includes a tof camera. This specific pointcloud can be colored by either confidence or intensity, since those values are included with the tof topic. 

![voxl-portal-tof-pointcloud.png](/images/voxl-sdk/voxl-portal/voxl-portal-tof-pointcloud.png)
