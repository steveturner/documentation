---
layout: default
title: VOXL Portal
parent: VOXL SDK
nav_order: 40
has_children: true
has_toc: true
permalink: /voxl-portal/
youtubeId: 9MC0nBM0BqQ
---

# VOXL Portal

The voxl-portal package provides an embedded webserver on VOXL. It enables camera inspection and other debug tools all via web browser interface at your VOXL's IP address.

To access voxl-portal, simply point your web browser to the IP address of VOXL http://<VOXL IP>/

[Source Code](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-portal)

{% include youtubePlayer.html id=page.youtubeId %}
