---
layout: default
title: VOXL PX4 User Guide
parent: VOXL PX4
nav_order: 10
permalink:  /voxl-px4-user-guide/
youtubeId: aVHBWbwp488
---

# VOXL PX4 User Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Introduction

### Block Diagram

<img src="/images/voxl2/m0054-px4-user-guide-overview-v1.png" alt="m0054-px4-user-guide-overview-v1"/>

[View in fullsize](/images/voxl2/m0054-px4-user-guide-overview-v1.png){:target="_blank"}

### Supported Devices

The `voxl-px4` package is supported on the following platforms:

- VOXL 2
- RB5 Flight

This guide is geared towards someone trying to use the PX4 system, and not necessarily interested in all the bits and bytes that make it work.  If you want to know the inner workings, please see the [VOXL PX4 Developer Guide](/voxl-px4-developer-guide/)

### Key PX4 Differences to Note

- PX4 installation and updates are installed on the companion computer via the `apt` package manager (not QGroundControl)
- Currently, the USBC connection does not support a connection to QGroundControl
- Natively, ESCs are control is via UART, if you need PWM, check out [voxl2-io](/voxl2-io)

### Quickstart Video

{% include youtubePlayer.html id=page.youtubeId %}

## Requirements to Use this Guide

### User Type

Although we are striving to remove this requirement, the user following this guide should have some minimal command line skills.  This means you should be semi-comfortable opening up a terminal and running some basic commands to use this guide.

### Minimum Setup

Minimum requirements are (e.g. to play around at your desk)

- VOXL 2 Development Kit (MDK-M0054-1-01)
  - VOXL 2 (MDK-M0054-1-00)
  - VOXL Power Module (MDK-M0041-1-B-00)
  - Power cable (MCBL-00009-1)
- Power supply 12V 3A ((MPS-00005-1), XT60) (or 2S-6S battery)
  - *Note: inrush current on bootup requires 30W power supply min, nominally ~8W at runtime in beta*
- Host PC with [Android Debug Bridge]()
- USBC cable

### Setup to Connect to Ground Control Station

Addtionally/Optionally (e.g. to connect to Ground Control Station):

- Add-on board for networking:
  - [USB Expansion Board](https://www.modalai.com/products/voxl-usb-expansion-board-with-debug-fastboot-and-emergency-boot) (M0017) + JST-to-USB cable (MCBL-00009-1) + [Ethernet](/voxl2-connecting-quickstart/) or [Wi-Fi dongle](/voxl2-wifidongle-user-guide/)
  - 5G Modem Add-on (M0090-3-01) + your own VPN (or email devops @ modalai.com if interested in possible beta programs)
  - [Microhard Modem Add-On](https://www.modalai.com/products/voxl-microhard-modem-usb-hub) (M0048-1)

### Setup for Bringup and Manual Flight

Addtionally/Optionally (e.g. to fly manual modes, testing and bringup):

- [ModalAI 4-in-1 UART ESC](https://www.modalai.com/products/voxl-esc) (M0049)
- GPS/Mag/Spektrum RC input wiring harness (MSA-D0006-1-00) - or make your own 12-pin JST GH connector ([pinouts](/voxl2-connectors/))
- the actual GPS/Mag/Spektrum RC* receiver hardware
- Frame/Motors/Props/etc

*Note: only Spektrum receivers supported in beta*

## Connections and Sensors

For detailed pinouts, please see the [VOXL2 datasheet](/voxl2-connectors/)

- J4 - Power Input and battery monitoring
- J9 - USBC, adb connection to host computer
- J18 - Connects to ModalAI 4-in-1 ESC using [MCBL-00029](/cable-datasheets/#mcbl-00029)

<img src="/images/voxl2/m0054-px4-user-guide-1.png" alt="m0054-px4-user-guide-1" width="1280"/>

For detailed pinouts, please see the [VOXL2 datasheet](/voxl2-connectors/)

- J19 - GPS Input (GNSS UART, Mag I2C) Spektrum RC Input, spare I2C (future, SLPI)
- J10 - future, external SPI
- J2 - fan, 5VDC, controllable in future
- IMU0 - U16 - ICM-42688p, accessible from PX4 (slpi_proc)
- IMU1 - U17 - ICM-42688p, accessible from Ubuntu (apps_proc)
- BARO0 - U18 - BMP-388 (0x76h) (not currently used)
- BARO1 - U19 - ICP-10100 (0x63h) (PX4, slpi_proc)

<img src="/images/voxl2/m0054-px4-user-guide-2.png" alt="m0054-px4-user-guide-2" width="1280"/>

*Connector colors are not blue...*

## Quickstart

### Known Issues and Limitations

- the MAVLink shell available through QGC doesn't currently work on VOXL 2.  Instead, use the USBC connection and adb to access the shell if needing to run commands

### Desktop Use Case

TODO

### Connect to Ground Station Use Case

TODO

### Flight Use Case

TODO


[Next: VOXL 2 PX4 Developer Guide](/voxl-px4-developer-guide/){: .btn .btn-green }