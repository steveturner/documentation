---
layout: default
title: VOXL Emulator Docker Image
parent: Build Environments
nav_order: 15
permalink: /voxl-emulator/
---

# VOXL Emulator Docker Image
{: .no_toc }


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

Develop for VOXL in a Docker running on your Linux PC! This tool lets you use the docker build environments described in the [build environments page](/build-environments/).


## Prerequisite

Follow the instructions to install Docker CE and the voxl-docker script [here](https://developer.modalai.com/asset/download/).



## Install the voxl-emulator Docker Image

### VOXL and VOXL Flight

voxl-emulator enables one to develop CPU-based applications for VOXL and VOXL Flight, then test CPU-only in an emulated environment.

Download archived docker image from ModalAI Developer portal (login required). Currently it is on version 1.7 [voxl-emulator_v1.7.tgz](https://developer.modalai.com/asset/download/75) from [developer.modalai.com](https://developer.modalai.com)

### VOXL 2

qrb5165-emulator enables one to develop CPU-based applications for VOXL 2, then test CPU-only in an emulated environment. It is an ARM 64 emulator with the VOXL 2 sysroot mounted. It has very limited application to end developers. It is for low-level service development for cross-compiling applications not yet supported by cross-compiling in [voxl-cross](/voxl-cross/)

Download archived docker image from ModalAI Developer portal (login required). Currently it is on version 1.4 [qrb5165-emulator_V1.4.tgz](https://developer.modalai.com/asset/download/94) from [developer.modalai.com](https://developer.modalai.com)