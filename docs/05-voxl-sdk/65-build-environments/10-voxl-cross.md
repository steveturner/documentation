---
layout: default
title: VOXL Cross Docker Image
parent: Build Environments
nav_order: 10
permalink: /voxl-cross/
---

# VOXL Cross Docker Image
{: .no_toc }


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

Develop for VOXL in a Docker running on your Linux PC! This tool lets you use the docker build environments described in the [build environments page](/build-environments/).


## Prerequisite

* [Install docker-ce](https://docs.docker.com/engine/install/ubuntu/)

## Build the voxl-cross Docker Image

Instructions to build and install voxl-cross can be found at the project's README file [here](https://gitlab.com/voxl-public/support/voxl-docker)

## Install pre-built voxl-cross

Download the pre-built voxl-cross [voxl-cross_V2.5.tar.gz](https://developer.modalai.com/asset/download/120) from https://developer.modalai.com/

```
$ docker load -i voxl-cross_V2.5.tar.gz
$ git clone https://gitlab.com/voxl-public/support/voxl-docker.git
$ cd voxl-docker/
$ ./install-voxl-docker-script.sh
$ voxl-docker -i voxl-cross:V2.5
```

To build an example project for VOXL

```
$ mkdir cross-workspace
$ cd cross-workspace
$ voxl-docker -i voxl-cross:V2.5
voxl-cross:~$ git clone https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-json.git
voxl-cross:~$ cd libmodal-json/
voxl-cross:~$ ./build.sh qrb5165

```