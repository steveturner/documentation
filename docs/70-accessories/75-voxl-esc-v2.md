---
layout: default3
title: VOXL ESC V2
nav_order: 75
has_children: true
parent: Accessories
permalink: /modal-escs/
thumbnail: /modal-esc/esc.png
buylink: https://www.modalai.com/products/voxl-esc
summary: Documentation for ModalAI's high-performance, closed-loop Electronic Speed Controllers (ESCs) that use a digital interface (UART, i2c in future).
---

# ModalAI Electronic Speed Controller (ESC)
{: .no_toc }

ModalAI's Electronic Speed Controllers (ESCs) are high-performing, closed-loop speed controllers that use a digital interface (UART, i2c in future).

<a href="https://www.modalai.com/products/voxl-esc" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/category/13/escs" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>

## Brief Overview
Brushless Electronic Speed Controllers (**ESCs**) are devices that consist of hardware
and software for controlling three-phase brushless DC (**BLDC**) motors. ESCs communicate
with the flight controller, which instructs the ESCs how fast the motor should spin.

ModalAI's ESCs implement the following advanced features:
* Full integration with ModalAI VOXL and Flight Core PCBs
* Bi-directional UART communication with checksum; status and fault monitoring
* Real-time status and health reporting at high update rate (100Hz+ each)
* Closed-loop RPM control for best flight performance
* LED control from flight controller via UART

## Feature Comparison

<table>
<tr>
  <td><center><img src="/images/modal-esc/m0027/m0027a-top.jpg" width="300"></center></td>
</tr>
</table>

| Feature                                                                                                 | ModalAI 4-in-1 ESC (M0049-1)                | ModalAI 4-in-1 ESC (M0117-1)                | ModalAI 4-in-1 ESC (M0117-3)                |
|---------------------------------------------------------------------------------------------------------|---------------------------------------------|---------------------------------------------|---------------------------------------------|
| Input Voltage                                                                                           | 5.5V-16.8V (2-4S Lipo)                      | 5.5V-16.8V (2-4S Lipo)                      | 5.5V-16.8V (2-4S Lipo)                      |
| Aux Power Output                                                                                        | 2x 4.5V (adjustable) 600mA                  | 2x 4.5V (adjustable) 600mA                  | 2x 4.5V (adjustable) 600mA                  |
| Max Continuous Current Per Motor                                                                        | 20A (thermally limited)                     | 20A (thermally limited)                     | 20A (thermally limited)                     |
| Max Burst Current Per Motor                                                                             | 40-50A (requires airflow and heat-spreader) | 40-50A (requires airflow and heat-spreader) | 40-50A (requires airflow and heat-spreader) |
| MCU                                                                                                     | STM32F051K86                                | STM32F051C6U6                               | STM32F051C6U6                               |
| MOSFET Driver                                                                                           | MP6530                                      | MP6530                                      | MP6531A                                     |
| MOSFETs                                                                                                 | AON7528 (N)                                 | AON7528 (N)                                 | AON7528 (N)                                 |
| Individual Current Sensing                                                                              | 4x 2mOhm + INA186                           | 4x 2mOhm + INA186                           | 4x 2mOhm + INA186                           |
| ESD signal protection                                                                                   | ✅                                          | ✅                                           | ✅                                           |
| Temperature Sensing                                                                                     | ✅                                          | ✅                                           | ✅                                           |
| On-board Status LEDs                                                                                    | ✅                                          | ✅                                           | ✅                                           |
| External LEDs                                                                                           | Neopixel LEDs                               | Neopixel LEDs                               | Neopixel LEDs                               |
| Secure Bootloader                                                                                       | Yes (AES256)                                | Yes (AES256)                                | Yes (AES256)                                |
| PWM Switching Frequency                                                                                 | 48, 24 Khz                                  | 48, 24 Khz                                  | 48, 24 Khz                                  |
| Maximum RPM (6 pole pairs)                                                                              | 50K+                                        | 50K+                                        | 50K+                                        |
| PWM control input                                                                                       | ✅                                          | ✅                                           | ✅                                           |
| Active Freewheeling                                                                                     | ✅                                          | ✅                                           | ✅                                           |
| Disable Regenerative Braking                                                                            | ❌                                          | ❌                                           | ✅                                           |
| Tone Generation                                                                                         | ✅                                          | ✅                                           | ✅                                           |
| Closed-loop RPM Control                                                                                 | Yes (10Khz)                                 | Yes (10Khz)                                 | Yes (10Khz)                                 |
| Number of UART ports                                                                                    | 2 (2Mbit+)                                  | 2 (2Mbit+)                                  | 2 (2Mbit+)                                  |
| [modal_io](https://github.com/PX4/PX4-Autopilot/tree/main/src/drivers/actuators/modal_io) UART Protocol | ✅                                          | ✅                                           | ✅                                           |
| Weight without wires (g)                                                                                | 9.5                                         | 9.5                                         | 9.5                                         |
| Board Dimensions (mm)                                                                                   | 40.5 x 40.5                                 | 40.5 x 40.5                                 | 40.5 x 40.5                                 |
| Mounting Hole Size, Pattern (mm)                                                                        | 3.05, 31.0 x 33.0                           | 3.05, 31.0 x 33.0                           | 3.05, 31.0 x 33.0                           |

## Mechanical Drawings

| PCB     | 3D STEP                                                                                                              |
|---------|----------------------------------------------------------------------------------------------------------------------|
| M0049-1 | [3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0049_ESC_4_IN_1_REVB_CAM_FINAL_20200610.stp) |
| M0117-1 | [3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0117_ESC_4_IN_1_AD_REVA(-1_MAIN).step)       |
