---
layout: default
title: M0024 IMX214 Module Datasheet
parent: Image Sensors
nav_order: 24
has_children: false
permalink: /M0024/
---

# VOXL Hires Sensor Datasheet

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Specification

### M0024-1 10cm IMX214 100° FOV  ([Buy Here](https://www.modalai.com/M0024))

| Specicifcation | Value |
| --- | --- |
| Sensor | IMX214 [Datasheet](https://www.mouser.com/datasheet/2/897/ProductBrief_IMX214_20150428-1289331.pdf) |
| Shutter | Rolling |
| Resolution | 4224x3200 |
| Framerate | up to 60Hz |
| Lens Size | 1/3.06" |
| Focusing Range | 5cm~infinity |
| Focal Length | 3.33mm |
| F Number | 2.75 |
| Fov(DxHxV) | ~100° |
| TV Distortion | < 6% |
| Weight | 3g |

#### M0024-1 Drawings

[2D Drawing](https://storage.googleapis.com/modalai_public/modal_drawings/M0024_2D_11-01-21.pdf)

## Module Connector Schematic for J2

![voxl-schematic-for-camera-module-to-connect-to-J2.png](../../images/other-products/image-sensors/voxl-schematic-for-camera-module-to-connect-to-J2.png)

## Pin Out

![Hi_Res_Pin1.JPG](/images/other-products/image-sensors/Hi_Res_Pin1.JPG)

![Hi_Res_Location.JPG](/images/other-products/image-sensors/Hi_Res_Location.JPG)

## Samples of Hires sensor on Sentinel.
### Indoor
![hires_in.jpg](/images/other-products/image-sensors/Samples/hires_in.jpg)
{:target="_blank"}

### Outdoor
![hires_out.jpg](/images/other-products/image-sensors/Samples/hires_out.jpg)
{:target="_blank"}