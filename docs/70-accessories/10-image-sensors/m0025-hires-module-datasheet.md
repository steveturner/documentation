---
layout: default
title: M0025 IMX214 Module Datasheet
parent: Image Sensors
nav_order: 25
has_children: false
permalink: /M0025/
---

# VOXL Hires Sensor Datasheet

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Specification

### M0025-1 8.5cm IMX214 100° FOV ([Buy Here](https://www.modalai.com/M0025))

| Specicifcation | Value                                                                                               |
|----------------|-----------------------------------------------------------------------------------------------------|
| Sensor         | IMX214 [Datasheet](https://www.mouser.com/datasheet/2/897/ProductBrief_IMX214_20150428-1289331.pdf) |
| Shutter        | Rolling                                                                                             |
| Resolution     | 4224x3200                                                                                           |
| Framerate      | up to 60Hz                                                                                          |
| Lens Size      | 1/3.06"                                                                                             |
| Focusing Range | 5cm~infinity                                                                                        |
| Focal Length   |                                                                                                     |
| F Number       |                                                                                                     |
| Fov(DxHxV)     | ~100°                                                                                               |
| TV Distortion  |                                                                                                     |
| Weight         | <1g                                                                                                 |


### M0025-2 8.5cm IMX214 81° FOV 

| Specicifcation | Value                                                                                               |
|----------------|-----------------------------------------------------------------------------------------------------|
| Sensor         | IMX214 [Datasheet](https://www.mouser.com/datasheet/2/897/ProductBrief_IMX214_20150428-1289331.pdf) |
| Shutter        | Rolling                                                                                             |
| Resolution     | 4224x3200                                                                                           |
| Framerate      | up to 60Hz                                                                                          |
| Lens Size      | 1/3.06"                                                                                             |
| Focusing Range | 5cm~infinity                                                                                        |
| Focal Length   |                                                                                                     |
| F Number       |                                                                                                     |
| Fov(DxHxV)     | 81.3° x 69.0° x 54°                                                                                 |
| TV Distortion  |                                                                                                     |
| Weight         | <1g                                                                                                 |

