---
layout: default3
title: VOXL CAM
nav_order: 25
has_children: true
permalink: /voxl-cam/
parent: VOXL Dev Kits
thumbnail: /voxl-cam/voxl-cam-front.png
buylink: https://www.modalai.com/products/voxl-cam
summary: VOXL CAM is a state-of-the art, fully integrated robot perception system
---

# VOXL-CAM
{: .no_toc }
VOXL CAM is a state-of-the art, fully integrated robot perception system

<a href="https://www.modalai.com/products/voxl-cam" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/category/21/voxl-cam" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>


![voxl-cam-r1-c1-index.png](/images/voxl-cam/voxlcam.jpg)

1. [VOXL-CAM Quickstart](/voxl-cam-quickstart/)
2. [VOXL-CAM User Guide](/voxl-cam-user-guide/)
3. [VOXL-CAM Datasheet](/voxl-cam-datasheet/)

## VOXL CAM V1 Versions and Options
{: .no_toc }


| Options                       | Description                                                     | Status             | SKU Configuration | 
|---                            |--                                                               |---                 |
---                |
| VOXL CAM Core Dev Kit                       | [VOXL](/voxl-datasheet/), Hi-res, Tracking                                        | Available          | C3             | 
| VOXL CAM Core Dev Kit + TOF                    | [VOXL](/voxl-datasheet/), ToF, Hires  Tracking                                         | Available        | C7              | 
| VOXL CAM Core Dev Kit + Flight Core  | [Flight Core](/flight-core-datasheet/), Hi-res, Tracking | Available        | C3-FC1-D1               |
| VOXL CAM Core Dev Kit + TOF + Flight Core  | [Flight Core](/flight-core-datasheet/), ToF, Tracking        | Available        | C7-FC1-D1               | 
| VOXL CAM Core Dev Kit + Flight Core + 4G Modem (NA) | [Flight Core](/flight-core-datasheet/), Hi-res, Tracking [LTE v2, WP7611](/lte-modem-and-usb-add-on-v2-datasheet/)       | Available        | C3-FC1-M8               |
| VOXL CAM Core Dev Kit + TOF + Flight Core + 4G Modem (NA) | [Flight Core](/flight-core-datasheet/), ToF, Tracking [LTE v2, WP7611](/lte-modem-and-usb-add-on-v2-datasheet/)                                                     | Available             | C7-FC1-M8 | 
|---                            |--                                                               |---                 |---                |
|VOXL CAM Core Dev Kit + Flight Core + 4G Modem (EU) | [Flight Core](/flight-core-datasheet/), Hi-res, Tracking [LTE v2, WP7607](/lte-modem-and-usb-add-on-v2-datasheet/)                                        | Available          | C3-FC1-M5             | 
| VOXL CAM Core Dev Kit + TOF + Flight Core + 4G Modem (EU) |[Flight Core](/flight-core-datasheet/), Hi-res, Tracking [LTE v2, WP7607](/lte-modem-and-usb-add-on-v2-datasheet/)                                         | Available          | C7-FC1-M5              |

