---
layout: default
title: VOXL 2 Flight Deck Connections
parent: VOXL 2 Flight Deck User Guide
nav_order: 5
has_children: false
permalink: /voxl2-flight-deck-userguide-connections/
---

# VOXL 2 Flight Deck Hardware Overview 
{: .no_toc }

This guide walks you through the main connections of the VOXL 2 Flight Deck in effort to assist you getting it setup for a vehicle.

For technical details, see the [datasheet](/voxl2-flight-deck-datasheet/).

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Hardware Overview:

### Image Sensors

[View in fullsize](/images/voxl2-flight-deck/voxl2-deck-labeled3.jpg){:target="_blank"}
<img src="/images/voxl2-flight-deck/voxl2-deck-labeled3.jpg" alt="voxl2-flight-deck-connections.png"/>

| Camera IDs | Description                                                        | 
|---         |---                                                                 |
| 0-1        |  ov7251, synchronized stereo pair, 640x480 85.6º diagonal,  30 FPS |
| 2          |  ov7251, 640x480 166º diagonal, 30 FPS               |
| 3          |  imx214, 4k, 30 FPS                                  |
| 4-5        |  ov7251, synchronized stereo pair, 640x480 85.6º diagonal,  30 FPS |

### Connectors

[View in fullsize](/images/voxl2-flight-deck/voxl2-deck-labeled1.jpg){:target="_blank"}

<img src="/images/voxl2-flight-deck/voxl2-deck-labeled1.jpg" alt="voxl2-flight-deck-connections.png"/>

[View in fullsize](/images/voxl2-flight-deck/voxl2-deck-labeled2.jpg){:target="_blank"}

<img src="/images/voxl2-flight-deck/voxl2-deck-labeled2.jpg" alt="voxl2-flight-deck-connections.png" width="80%">

| Connector | Description         | MPN (Board Side)         | Mating MPN (Board/Cable Side) | Type                                | Signal Feature Summary                                                                                                                                                                                     |
|-----------|---------------------|--------------------------|-------------------------------|-------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| J3        | Legacy B2B          | QSH-030-01-L-D-K-TR      | QTH-030-01-L-D-A-K-TR         | B2B Receptacle, 60-pin              | 5V/3.8V/3.3V/1.8V power for plug-in boards, JTAG and Debug Signals, QUP expansion, GPIOs, USB3.1 Gen 2 (USB1)                                                                                              |
| J4        | Prime Power In      | 22057045                 | 0050375043                    | Cable Connector, 4-pin R/A          | +5V main DC power in + GND, I2C@5V for power monitors                                                                                                                                                      |
| J5        | High Speed B2B      | ADF6-30-03.5-L-4-2-A-TR  | ADM6-30-01.5-L-4-2-A-TR       | B2B Socket, 120-pin                 | More 3.8V/3.3V/1.8V power for plug-in boards, 5V power in for “SOM Mode”, QUP expansion, GPIOS (including I2S), SDCC (SD Card V3.0), UFS1 (secondary UFS Flash), 2L PCIe Gen 3, AMUX and SPMI PMIC signals |
| J9        | USB-C (ADB)         | UJ31-CH-3-SMT-TR         | USB Type-C                    | Cable Receptacle, 24-pin R/A        | ADB USB-C with re-driver and display port alternate mode (USB0)                                                                                                                                            |
| J10       | SPI Expansion       | SM08B-GHS-TB(LF)(SN)     | GHR-08V-S                     | Cable Header, 8-pin R/A             | SPI@3.3V with 2 CS_N pins, 32kHz CLK_OUT@3.3V                                                                                                                                                              |
| J12        | Fan                 | SM02B-SRSS-TB(LF)(SN)    | SHR-02V-S                     | Cable Header, 2-pin R/A             | 5V DC for FAN + PWM Controlled FAN-Return (GND)                                                                                                                                                            |
| J18       | ESC                 | SM04B-GHS-TB(LF)(SN)     | GHR-04V-S                     | Cable Header, 4-pin R/A             | ESC UART@3.3V, 3.3V reference voltage                                                                                                                                                                      |
| J19       | GNSS/MAG/RC/I2C            | SM12B-GHS-TB(LF)(SN)     | GHR-12V-S                     | Cable Header, 6-pin R/A             | GNSS UART@3.3V, Magnetometer I2C@3.3V, 5V, RC UART, Spare I2C                                                                                                                                                                  |

## Buttons

### SW1 - Force Fastboot Button

Force Fastboot momentary button.

To force device into fastboot mode:

- power off device, remove USB cable to completely power down
- press and hold SW1 button down
- power on device, attach USB cable
- release SW1 button
- from host computer, run `fastboot devices` and verify the device shows up.  If not, restart this procedure

To reboot device to fastboot:

- device is powered on
- press and hold SW1 for 30 seconds until the device reboots into fastboot mode

<hr>

### SW2 - EDL Switch

Emergency Download switch, used for factory flashing.  Should be left `OFF`.  See [user guide for QDL](/Qualcomm-Flight-RB5-QDL/) if interested in more information.

<hr>



### VOXL 2 Overview

VOXL 2 Flight Deck is powered by VOXL2. For more information, see [Here](/voxl-2/)

[View in fullsize](/images/voxl2/m0054-exploded-v1.3-rev0.jpg){:target="_blank"}
<img src="/images/voxl2/m0054-exploded-v1.3-rev0.jpg" alt="m0054-exploded" width="1280"/>

<hr>

[Next: Setup](/voxl2-flight-deck-userguide-setup){: .btn .btn-green }

