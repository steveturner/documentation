---
layout: default
title: VOXL CAM Modem Setup
parent: VOXL CAM User Guide
nav_order: 10
permalink: /voxl-cam-user-guide-modem/
---

# **VOXL-CAM v1 Modem Setup**
{: .no_toc }


![Voxl-cam-modem](/images/voxl-cam/voxl-cam-modem.png)

<p style="color:darkgreen"> <b> This guide is for VOXL-CAM with Modem </b> <br>
(SKU C3-FC1-M8, C7-FC1-M8, C3-FC1-M5, C7-FC1-M5)  </p>

---

More more details, check out our Modem docs [Here](https://docs.modalai.com/4G-LTE-Modems/) for more details.

---

<details open markdown="block">
  <summary>
    Table of Contents
  </summary>
  {: .text-echo }
1. [Datasheet, v1](/voxl-cam-v1-datasheet/)
2. Second Step
{:toc}
</details>


# **VOXL CAM Setup with Modem**
{: .text-alpha}

## Install SIM Card  
See [here](/lte-v2-modem-user-guide/)


## Configure Modem 
See [here](/lte-v2-modem-user-guide/)
