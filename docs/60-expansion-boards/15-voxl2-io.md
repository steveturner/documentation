---
layout: default3
title: VOXL 2 IO
nav_order: 15
parent: Expansion Boards
has_children: true
permalink: /voxl2-io/
thumbnail: /voxl2-io/voxl2-io.png
buylink: https://www.modalai.com/products/voxl2-io
summary: VOXL 2 IO is a PX4 IO based board that adds PWM output along with S.BUS radio and Spektrum radio connections to VOXL or VOXL 2.
---

# VOXL 2 IO
{: .no_toc }

VOXL 2 IO is a PX4 IO based board that is meant to be paired with a flight controller like VOXL 2 and adds PWM output along with S.BUS radio and Spektrum radio connections.  It communicates over UART to the flight controller running PX4.

<a href="https://www.modalai.com/products/voxl2-io" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>


![m0065-zoom](/images/voxl2-io/voxl2-io.png)

