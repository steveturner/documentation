---
layout: default
title: VOXL 2 Custom Expansion PCB
nav_order: 90
parent: Expansion Boards
has_children: true
permalink: /voxl2-custom-expansion/
summary: Information about custom PCBA's
---

# VOXL 2 Custom Expansion PCB
{: .no_toc }

If you are comfortable designing and manufacturing custom PCBAs, an example add-on board for VOXL 2 with reference Altium schematic package can be downloaded [here](https://storage.googleapis.com/modalai_public/modal_drawings/VOXL2_PLUGIN_STARTER_KIT_REVA.zip)

For those without Altium, if you cannot get an online conversion or the 30-day trial period to work for you, here is another zip file of the generated outputs (SCH, BOM, 3D Step, ODB++, and an ASSY drawing with all relevant dimensions): [Altium Generated Outputs](https://storage.googleapis.com/modalai_public/modal_drawings/VOXL2_PLUGIN_STARTER_KIT_REVA_OUTPUTS.zip)
