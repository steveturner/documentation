---
layout: default
title: VOXL 2 IO Datasheet
parent: VOXL 2 IO
nav_order: 10
has_children: false
permalink: /voxl2-io-datasheet/
---

# VOXL 2 IO Datasheet
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Overview
 
<img src="/images/voxl2-io/m0065-system-v0.png" alt="m0065-system" width="1280"/>

## Development Kits

| PN           | Description |
|---           |---          |
| MDK-M0065-00 | VOXL 2 IO board only |
| MDK-M0065-01 | VOXL 2 IO board, VOXL2 to VOXL2 IO UART Cable (MCBL-00061), SBus and Spektrum RC cables (MCBL-00021 and MCBL-00005) |
| MDK-M0065-02 | Same as MDK-M0065-01, plus PWM breakout Board and Cable (MCCA-M0022, MCBL-00004) |

## Dimensions

### 3D Drawings

[3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/M0065_PX4IO_PWM_REVA.step)

### 2D Drawings

25mm x 33mm x 7.2mm

<img src="/images/voxl2-io/m0065-2d.png"/>

## Features

| Feature          | Details                                                                                                                                                         |
|:-----------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Weight           | 4 g                                                                                                                                                              |
| MCU              | 72MHz, 32-bit ARM M3 [STM32F103C8T6](https://www.st.com/en/microcontrollers-microprocessors/stm32f103c8.html)                                                   |
| Memory           | 20Kb RAM                                                                                                                                                        |
|                  | 64Kbit Flash                                                                                                                                                    |
| Firmware         | [PX4](https://github.com/modalai/px4-firmware/tree/voxl2-io-1.11)                                                                                               |
| Inputs           | S.Bus                                                                                                                                                           |
|                  | Spektrum                                                                                                                                                        |
| Outputs          | 3 LEDs (1xRGB)                                                                                                                                                  |
|                  | 8 PWM Channels                                                                                                                                                  |

[Top](#table-of-contents)

## Block Diagram

![m0065-block-diagram.png](/images/voxl2-io/m0065-block-diagram.png)
*Figure 1*
{: style="text-align: center;"}


## Connector Callouts

<img src="/images/voxl2-io/m0065-connectors-v0.png" alt="m0065-connectors" width="1280"/>

<hr>

### J1 - PWM Output

| Connector | MPN |
| --- | --- | 
| Board Connector | BM10B-GHS-TBT(LF)(SN)(N) |
| Mating Connector |  |

| Pin #      | Signal Name           | Usage / Notes                                 |
| ---        | ---                   | ---                                           |
| 1          | 5P0V_BOOST            | Default on, no control unless R10 stuffed     |
| 2          | IOPWM_OUT_CH1         |                                               |
| 3          | IOPWM_OUT_CH2         |                                               |
| 4          | IOPWM_OUT_CH3         |                                               |
| 5          | IOPWM_OUT_CH4         |                                               | 
| 6          | IOPWM_OUT_CH5         |                                               | 
| 7          | IOPWM_OUT_CH6         |                                               |
| 8          | IOPWM_OUT_CH7         |                                               |
| 9          | IOPWM_OUT_CH8         |                                               |
| 10         | GND                   |                                               |

<hr>

### J2 - IO MCU Spektrum UART

| Connector | MPN |
| --- | --- | 
| Board Connector | BM10B-GHS-TBT(LF)(SN)(N) |
| Mating Connector |  |

Color: Yellow

| Pin #      | Signal Name           | Usage / Notes                                 |
| ---        | ---                   | ---                                           |
| 1          | 3P3V_IO_SPEKTRUM      | Controllable                                  |
| 2          | SPEKTRUM_UART_TX      |                                               |
| 3          | SPEKTRUM_UART_RX      |                                               |
| 4          | GND                   |                                               |

<hr>

### J3 - IO MCU S.BUS UART

| Connector | MPN |
| --- | --- | 
| Board Connector | BM10B-GHS-TBT(LF)(SN)(N) |
| Mating Connector |  |

Color: Black

| Pin #      | Signal Name           | Usage / Notes                                 |
| ---        | ---                   | ---                                           |
| 1          | 5P0V_BOOST            | Default on, no control unless R10 stuffed     |
| 2          | SBUS_OUTPUT_INV       |                                               |
| 3          | SBUS_INPUT_INV        |                                               |
| 4          | GND                   |                                               |

<hr>

### J4 - Host UART Input Connector

| Connector | MPN |
| --- | --- | 
| Board Connector | BM10B-GHS-TBT(LF)(SN)(N) |
| Mating Connector |  |

Color: White

| Pin #      | Signal Name             | Usage / Notes                                 |
| ---        | ---                     | ---                                           |
| 1          | 3P3V_IO                 | Main voltage input from host                  |
| 2          | HOST_UART_TX_TO_IOMCU   |                                               |
| 3          | HOST_UART_RX_FROM_IOMCU |                                               |
| 4          | GND                     |                                               |

<hr>

### J5 - STM Programming Header

| Connector | MPN |
| --- | --- | 
| Board Connector | BM08B-SRSS-TBT(LF)(SN) |
| Mating Connector |  |

| Pin #      | Signal Name           | Usage / Notes                                 |
| ---        | ---                   | ---                                           |
| 1          | 3P3V_IO               | Jlink, STLink, etc                            | 
| 2          | UART_2W_DEBUG_TX      | nsh                                           |
| 3          | UART_2W_DEBUG_RX      | nsh                                           |
| 4          | IOMCU_SWDIO           | Jlink, STLink, etc                            | 
| 5          | IOMCU_SWCLK           | Jlink, STLink, etc                            | 
| 6          | GND                   |                                               |
| 7          | PROG_RESET_N          | Jlink, STLink, etc                            | 
| 8          | VPP_STM               | STM BOOT0                                     |
