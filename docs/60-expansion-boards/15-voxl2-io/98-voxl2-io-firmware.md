---
layout: default
title: VOXL 2 IO Firmware
parent: VOXL 2 IO
nav_order: 98
has_children: false
permalink: /voxl2-io-firmware/
---

# VOXL 2 IO Firmware / Bootloader
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Summary

### Bootloader

#### Build

| Component    | Notes   |
|---           |---      |
| Build Docker | https://gitlab.com/voxl-public/flight-core-px4/px4-bootloader-build-docker |
| Source Code  | https://github.com/PX4/PX4-Bootloader.git |

To build:

```bash
git clone https://gitlab.com/voxl-public/flight-core-px4/px4-bootloader-build-docker
cd px4-bootloader-build-docker
./docker-build-image.sh

cd ..
git clone -b https://github.com/PX4/PX4-Bootloader.git --recursive
cd px4-bootloader
cp ../px4-bootloader-build-docker/docker-run-image.sh .

./docker-run-image.sh
make modalai_voxl2_io_bl -j4
```

Output at `build/modalai_voxl2_io_bl/modalai_voxl2_io_bl.bin`

#### Flash

Write the 4kb bootloader to `0x08000000`

### Application

#### Build

| Component    | Notes   |
|---           |---      |
| Build Docker | https://gitlab.com/voxl-public/flight-core-px4/px4-stm32-build-docker.git |
| Source Code  | https://github.com/modalai/px4-firmware/tree/voxl2-io |

To build:

```bash
https://gitlab.com/voxl-public/flight-core-px4/px4-stm32-build-docker.git
cd px4-stm32-build-docker
./docker-build-image.sh

cd ..
git clone -b voxl2-io-1.11 https://github.com/modalai/px4-firmware --recursive
cd px4-firmware
cp ../px4-stm32-build-docker/docker-run-image.sh .

./docker-run-image.sh
make modalai_voxl2-io
```

Output at `build/modalai_voxl2-io/modalai_voxl2_io.bin`

#### Flash

Write the 60kb application to `0x08001000`
