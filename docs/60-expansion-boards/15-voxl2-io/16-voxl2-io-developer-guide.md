---
layout: default
title: VOXL 2 IO Developer Guide
parent: VOXL 2 IO
nav_order: 16
has_children: true
permalink: /voxl2-io-developer-guide/
---

# VOXL 2 IO Developer Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Overview

### Hardware

<img src="/images/voxl2-io/m0065-dev-guide-bd-0.jpg" alt="m0065-dev-guide-bd-0"/>

- A battery powers VOXL2 (`M0054`) through a Power Module (`M0041`), and the battery also powers the ESCs/Motors
- VOXL2 J19 connects to VOXL2 IO J4 to provide both power and communications using a UART
- VOXL2 IO connects to an RC receiver (either J2 or J3, depending on user's receiver type)
- VOXL2 IO J1 connects to a PWM breakboard board (`M0022`) and 'communicates' to the ESCs using PWM, which in turn spin the motors


### Software

<img src="/images/voxl2-io/m0065-dev-guide-bd-1.jpg" alt="m0065-dev-guide-bd-1"/>

#### High Level Idea

VOXL2 runs PX4 and VOXL2 IO runs PX4IO.  They share a register map and keep it synchronized.  The VOXL2 IO has a control loop that includes mixing, and reacts to updates that are from VOXL2s `px4io` driver which populates the register map based updates to subscriptions.

#### Rumning on VOXL2

- On VOXL2 (`M0054`), the `px4io` driver runs on the sDSP/SLPI proc
- The `px4io` driver creates a bit of a hybrid device which has a few similarities as `pwm_output` device
- The `voxl-px4.config` script starts the detection and configuration process, including loading a mixer, which actually runs on VOXL2 IO (`M0065`)
- The `px4io` driver subscribes to various topics and updates the 'registers' on the VOXL2 IO board by sending commands over UART following a simple [protocol](https://github.com/PX4/PX4-Autopilot/blob/main/src/modules/px4iofirmware/protocol.h)

#### Rumning on VOXL2 IO

- Upon bootup, VOXL2 (`M0054`) detects the VOXL2 IO (`M0065`) board, and configures the VOXL2 IO board including loading of a mixer
- A control loop takes place, which involves checking various flags, updating status based upon updates from VOXL2, checking for inputs, handling outputs, etc