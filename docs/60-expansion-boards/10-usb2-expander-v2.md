---
layout: default
title: USB2 Expander V2
parent: Expansion Boards
nav_order: 10
permalink: /usb-epxansion-with-fastboot-v2-datasheet/
thumbnail: /other-products/voxl-usb-add-on-v2/m0078-2-iso-view.png
buylink: https://www.modalai.com/products/voxl-debug-board-v2
summary: VOXL USB Expander v2 is a revised version of v1 with more features in a smaller and lighter package for VOXL and VOXL 2.
---

# USB2 Expansion Board with Fastboot V2 Datasheet
{: .no_toc }

<a href="https://www.modalai.com/products/voxl-debug-board-v2" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>



## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}


## Overview

The USB Expansion Board with Fastboot v2 is used to add a USB 2.0 port to VOXL/VOXL2 and provides a Fastboot button for forcing devices into Fastboot mode when needed.
 
<img src="/images/other-products/voxl-usb-add-on-v2/m0078-hero.png" alt="m0078-hero"/>

### Dimensions

#### 3D Drawings

[Download](https://storage.googleapis.com/modalai_public/modal_drawings/M0078-2_FINAL.step)

<img src="/images/other-products/voxl-usb-add-on-v2/m0078-2-iso-view.png">

#### 2D Drawings

<img src="/images/other-products/voxl-usb-add-on-v2/m0078-dimensions.png">


### Features

| Feature      | Details                    |
|-:------------|-:--------------------------|
| Weight       | TODO                       |
| USB 2.0 port | QTY1 (500mA or 5.0A power) |

### Revisions

| PN             | Information                                                  |
|----------------|--------------------------------------------------------------|
| MDK-M0078-2-01 | Standard version, no EDL switch (S2), no serial console (J5) |
| MDK-M0078-1-01 | Install S2, install FTDI IC and J5                           |

### Schematic

[M0078 Schematic](https://storage.googleapis.com/modalai_public/modal_schematics/M0078_A_SCHEMATIC.pdf)

## Connector Callouts

<img src="/images/other-products/voxl-usb-add-on-v2/m0078-callouts.png" alt="m0078-callouts"/>

### J2 - USB 5V Power Source Select

| Connector | MPN |
| --- | --- | 
| Board Connector | TSM-103-02-X-SH |

| Pin #      | Signal Name           |
|---         |---                    |
| 1          | 5P0V_LOCAL (2A)       |
| 2          | Connects to J3 pin 1  |
| 3          | 5V_VBUS (500mA)       |

<img src="/images/other-products/voxl-usb-add-on-v2/m0078-user-power-options-3.png" alt="m0078-user-power-options"/>

### J3 - USB 2.0

| Connector | MPN |
| --- | --- | 
| Board Connector | BM04B-GHS-TBT(LF)(SN)(N) |

| Pin #      | Signal Name           |
|---         |---                    |
| 1          | 5P0V                  |
| 2          | USB_M                 |
| 3          | USB_P                 |
| 4          | GND                   |

### J5 - NOT INSTALLED

NOT INSTALLED

| Connector | MPN |
| --- | --- | 
| Board Connector | BM04B-SRSS-TB(LF)(SN) |

| Pin #      | Signal Name |
|---         |---          |
| 1          | 3P3V        |
| 2          | RX_3P3V     |
| 3          | TX_3P3V     |
| 4          | GND         |

<hr>

### J1 - VOXL/VOXL2 Board To Board Connector

See host board datasheet for mating connector pinout.

<hr>

### S1 - Fastboot Button

Momentary push button for Fastboot.  Press and hold while booting up device, then let go after the device powers on, to enter fastboot.

### S2 - NOT INSTALLED

NOT INSTALLED

<hr>
