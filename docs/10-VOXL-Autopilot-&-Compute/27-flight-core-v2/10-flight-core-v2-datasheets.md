---
layout: default
title: Flight Core v2 Datasheets
parent: Flight Core v2
nav_order: 10
has_children: true
permalink: /flight-core-v2-datasheets/
---

# Flight Core Datasheets
{: .no_toc }

<img src="/images/flight-core-v2/m0087-hero.png" width="320"/>

The ModalAI Flight Core v2 is an STM32**H7**-based flight controller for PX4, made in the USA.  The Flight Core v2 can be paired with VOXL for obstacle avoidance and GPS-denied navigation, or used independently as a standalone flight controller.
