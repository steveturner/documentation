---
layout: default
title: VOXL 2 Onboard Sensors
parent: VOXL 2 Datasheets
nav_order: 4
permalink:  /voxl2-onboard-sensors/
youtubeId: 2-MWy2tcMJg
---

# VOXL 2 Onboard Sensors
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## IMUs

Note the XYZ axis as drawn respresent the IMU data as reported by the voxl-imu-server MPA service, not the physical orientation of the IMU on the PCB. This aligns with the FRD reference frame when mounted on a drone in the typical orientation such as on the Sentinel reference drone.

![voxl2-imu-locations](../../images/voxl2/m0054_voxl2_imu_locations_with_axis.png)

| Name / Designator           | Description    | Interface                   | MPA Pipe          |
|---                          |---             |---                          |---                |
| PX4 IMU / IMU0 / U16        | TDK ICM42688p  | SSC_QUP5, SPI, SLPI (sDSP)  | /run/mpa/imu_px4  |
| Apps Proc IMU / IMU1 / U17  | TDK ICM42688p  | /dev/spidev3.0, SPI         | /run/mpa/imu_apps |



## Barometers

<img src="/images/voxl2/m0054-onboard-sensors-baro.png" alt="m0054-onboard-sensors-baro" width="1280"/>

| Name / Designator           | Description    | Interface                                |
|---                          |---             |---                                       |
| PX4 Baro 0 / BARO0 / U18    | BMP388         | SSC_QUP4, I2C, SLPI (sDSP), Addr: 0x76h  |
| PX4 Baro 0 / BARO1 / U19    | TDK ICP-10100  | SSC_QUP4, I2C, SLPI (sDSP), Addr: 0x63h  |

Related video:

{% include youtubePlayer.html id=page.youtubeId %}
