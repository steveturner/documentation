---
layout: default
title: VOXL 2 Camera Configs
parent: VOXL 2 Datasheets
nav_order: 3
permalink:  /voxl2-camera-configs/
youtubeiId2: J7iYorzm5rs
---

# VOXL 2 Camera Configurations
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

VOXL 2 has 3 camera groups (shown below), where each group has:

- QTY-2 full 4 lane MIPI CSI ports
- CCI and camera control signals
- 8 power rails (from 1.05V up to 5V) for cameras and other sensors
- Dedicated SPI bus

<img src="/images/voxl2/m0054-iamge-sensor-groups-v2.png" alt="m0054-image-sensors-groups">

## Available Configurations


### Platform Release 0.9.5

- SDK 0.9.5
- System Image 1.5.3

#### Changes

- Added IMX678 support 1920x1080@30, 3840x2160@30, on HW Sensor ID 4 (requires [M0076](/M0076/) interposer)
- Enabled IMX214/412/577 on HW Sensor ID 4 (requires [M0076](/M0076/) interposer).
- Add support for 90 Hz OV7251, on HW Sensor ID 2


#### 1.5.3 Supported Sensor Hardware IDs

| HW Sensor ID | Sensor Type                   | Description |
|:-------------|:------------------------------|:------------|
| 0            | OV7251 [M0015](/M0015/)       | Stereo     |
| 0            | OV9782 [M0113](/M0113/)       | Stereo      |
| 0            | PMD TOF [M0040](/M0040/)      | ToF     |

| HW Sensor ID | Sensor Type              | Description |
|:--------------|:--------------------------|:-------------|
| 1            | OV7251 [M0015](/M0015/)  | Stereo      |
| 1            | OV9782 [M0113](/M0113/)  | Stereo      |
| 1            | PMD TOF [M0040](/M0040/) | ToF     |

| HW Sensor ID | Sensor Type                   | Description |
|:--------------|:-------------------------------|:-------------|
| 2            | OV7251 [M0014](/M0014/)       | Tracking      |
| 2            | ov9782                        | Stereo      |
| 2            | PMD TOF [M0040](/M0040/)      | Depth     |

| HW Sensor ID | Sensor Type                   | Description |
|:--------------|:-------------------------------|:-------------|
| 3            | IMX214 [M0025-2](/M0025/)     | Hires      |
| 3            | IMX577/412 [M0061-1](/M0061/) | Hires      |
| 3            | PMD TOF [M0040](/M0040/)      | ToF     |

| HW Sensor ID | Sensor Type              | Description |
|:--------------|:--------------------------|:-------------|
| 4            | OV7251 [M0015](/M0015/)    | CV,BW,      |
| 4            | OV9782 [M0113](/M0113/)    | Stereo      |
| 4            | PMD TOF [M0040](/M0040/)   | ToF     |
| 4            | IMX678 [M0061-2](/M0061/)  | Hires      |
| 4            | IMX577/412 [M0061-1](/M0061/) | Hires      |
| 4            | IMX214 [M0025-2](/M0025/)     | Hires      |

| HW Sensor ID | Sensor Type              | Description |
|:--------------|:--------------------------|:-------------|
| 5            | OV7251 [M0015](/M0015/)  | Stereo      |
| 5            | OV9782 [M0113](/M0113/)  | Stereo      |
| 5            | PMD TOF [M0040](/M0040/) | ToF     |

#### 1.5.3 Supported Sensor Settings

Some sensors may list additional capabilities when probed from software.  Below are what we've tested against, other settings may have unexpected results.

| Sensor                        | Sensor Type         | Resolution @ FPS                                                                                        |
|:-------------------------------|:---------------------|:---------------------------------------------------------------------------------------------------------|
| IMX678 [M0061-2](/M0061/)     | Hires               | 3840x2160 @ 30 <br> 1920x1080 @ 30                                                                      |
| IMX412/577 [M0061-1](/M0061/) | Hires               | 1920x1080 @ 30                                                                                          |
| IMX214 [M0025-2](/M0025/)     | Hires               | 4208 x 3120 @ 30 <br> 4096 x 2160 @ 30 <br> 2104 x 1560 @ 30 <br> 1920 x 1080 @ 30 <br> 1280 x 720 @ 30 |
| PMD TOF [M0040](/M0040/)      | ToF                 | 224 x 1557 @ 5                                                                                          |
| OV7251 [M0014](/M0014/)       | Tracking Config     | 640x480 @ 30,60,90                                                                                      |
| OV7251 [M0015](/M0015/)       | Stereo Config Left  | 640x480 @ 30                                                                                            |
| OV7251 [M0015](/M0015/)       | Stereo Config Right | 640x480 @ 30                                                                                            |
| ov9782                        | Tracking Config     | 1280x800 @ 30                                                                                           |
| OV9782 [M0113](/M0113/)       | Stereo Config Left  | 1280x800 @ 30                                                                                           |
| OV9782 [M0113](/M0113/)       | Stereo Config Right | 1280x800 @ 30                                                                                           |


### Platform Release 0.9

- SDK 0.9
- System Image 1.4.1

#### 1.4.1 Supported Sensor Hardware IDs

| HW Sensor ID | Sensor Type             | Description |
|:--------------|:-------------------------|:-------------|
| 0            | OV7251 [M0015](/M0015/) | Stereo      |
| 0            | OV9782 [M0113](/M0113/) | Stereo      |
| 0            | PMD TOF [M0040](/M0040/)| ToF     |

| HW Sensor ID | Sensor Type              | Description |
|:--------------|:--------------------------|:-------------|
| 1            | OV7251 [M0015](/M0015/)  | Stereo      |
| 1            | OV9782 [M0113](/M0113/)  | Stereo      |
| 1            | PMD TOF [M0040](/M0040/) | ToF     |

| HW Sensor ID | Sensor Type              | Description |
|:--------------|:--------------------------|:-------------|
| 2            | OV7251 [M0014](/M0014/)  | Tracking      |
| 2            | ov9782                   | Stereo      |
| 2            | PMD TOF [M0040](/M0040/) | ToF     |

| HW Sensor ID | Sensor Type                   | Description |
|:--------------|:-------------------------------|:-------------|
| 3            | IMX214 [M0025-2](/M0025/)     | Hires      |
| 3            | IMX577/412 [M0061-1](/M0061/) | Hires      |
| 3            | PMD TOF [M0040](/M0040/)      | ToF     |


| HW Sensor ID | Sensor Type              | Description |
|:--------------|:--------------------------|:-------------|
| 4            | OV7251 [M0015](/M0015/)  | CV,BW,      |
| 4            | OV9782 [M0113](/M0113/)  | Stereo      |
| 4            | PMD TOF [M0040](/M0040/) | ToF     |

| HW Sensor ID | Sensor Type              | Description |
|:--------------|:--------------------------|:-------------|
| 5            | OV7251 [M0015](/M0015/)  | Stereo      |
| 5            | OV9782 [M0113](/M0113/)  | Stereo      |
| 5            | PMD TOF [M0040](/M0040/) | ToF     |

#### 1.4.1 Supported Sensor Settings

| Sensor                        | Sensor Type         | Resolution @ FPS                                                                                        |
|:-------------------------------|:---------------------|:---------------------------------------------------------------------------------------------------------|
| IMX214 [M0025-2](/M0025/)     | Hires Config        | 4208 x 3120 @ 30 <br> 4096 x 2160 @ 30 <br> 2104 x 1560 @ 30 <br> 1920 x 1080 @ 30 <br> 1280 x 720 @ 30 |
| IMX412/577 [M0061-1](/M0061/) | Hires Config        | 1920x1080 @ 30                                                                                          |
| PMD TOF [M0040](/M0040/)      | ToF                 | 224 x 1557 @ 5                                                                                          |
| OV7251 [M0014](/M0014/)       | Tracking Config     | 640x480 @ 30                                                                                            |
| OV7251 [M0015](/M0015/)       | Stereo Config Left  | 640x480 @ 30                                                                                            |
| OV7251 [M0015](/M0015/)       | Stereo Config Right | 640x480 @ 30                                                                                            |
| ov9782                        | Tracking Config     | 1280x800 @ 30                                                                                           |
| OV9782 [M0113](/M0113/)       | Stereo Config Left  | 1280x800 @ 30                                                                                           |
| OV9782 [M0113](/M0113/)       | Stereo Config Right | 1280x800 @ 30                                                                                           |

## Quickstart Video

The following video demonstrates setting up the image sensors for the "C11" VOXL 2 development kit:

{% include youtubePlayer.html id=page.youtubeiId2 %}

## How to Configure

VOXL 2 ships with the VOXL SDK, which has the `voxl-configure-cameras` command.

## Image Sensor Configurations

### C11 - Front Stereo, Rear Stereo, Hires, and Tracking

<img src="/images/voxl2/m0054-dual-stereo-hires-tracking.jpg" alt="m0054-dual-stereo-hires-tracking" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0          | OV7251 [M0015](/M0015/)             | Fsync in0    | Front stereo |
| 1          | OV7251 [M0015](/M0015/)             | Fsync out0   | Front stereo |
| 2          | ov7251              |               | Tracking     |
| 3          | imx214, imx412      |               | Hires        |
| 4          | OV7251 [M0015](/M0015/)             | Fsync in1    | Rear stereo  |
| 5          | OV7251 [M0015](/M0015/)             | Fsync out1   | Rear stereo  |

### C3 - Front Stereo, Hires, and Tracking

<img src="/images/voxl2/m0054-front-stereo-hires-tracking.jpg" alt="m0054-front-stereo-hires-tracking" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0          | OV7251 [M0015](/M0015/)             | Fsync in0    | Front stereo |
| 1          | OV7251 [M0015](/M0015/)             | Fsync out0   | Front stereo |
| 2          | ov7251              |              | Tracking     |
| 3          | imx214, imx412    |              | Hires        |

### C10 - Front Stereo Only

<img src="/images/voxl2/m0054-front-stereo.jpg" alt="m0054-front-stereo" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0          | OV7251 [M0015](/M0015/)             | Fsync in0    | Front stereo |
| 1          | OV7251 [M0015](/M0015/)             | Fsync out0   | Front stereo |

### C4 - Tracking and Hires Only

<img src="/images/voxl2/m0054-hires-tracking.jpg" alt="m0054-hires-tracking" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0          | ov7251              |              | Tracking     |
| 1          | imx214, imx412 |              | Hires        |

### C6 - Hires + ToF + Tracking

Available starting with [Platform Release 0.9, sys img 1.4.1](/voxl2-system-image/#141-voxl2_platform_09targz).

**HW Setup**

<img src="/images/voxl2/m0054-hires-tof-tracking.jpg" alt="m0054-hires-tof-tracking" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0          | PMD TOF [M0040](/M0040/)             |              | Depth 0      |
| 1          | ov7251              |              | Tracking     |
| 2          | imx214, imx412      |              | Hires        |

### C8 - Hires Only

Available starting with [Platform Release 0.9.5, sys img 1.5.3](/voxl2-system-image/#changelog).

[M0061-2](/M0061/) based IMX678, on M0054 J8 (no other camera location for IMX678 is supported in 1.5.3, must be J8).

Shown here, the [M0076](/M0076/) interposer plugs into M0054 J8.  The [M0074](/M0074/) flex then connects to the [M0061-2](/M0062/) backpack for the IMX678 module.

<img src="/images/voxl2/m0054-imx678-m0061-2.JPG" alt="m0054-imx678-m0061-2" width="1280"/>

[M0025-2](/M0025/) based IMX214, on M0054 J8.

<img src="/images/voxl2/m0054-imx214-m0025-2.JPG" alt="m0054-imx214-m0025-2" width="1280"/>


**SW Setup**

```bash
voxl2:/$ voxl-configure-cameras 8
```

### C9 - Time of Flight (ToF) Only

Available starting with [Platform Release 0.9, sys img 1.4.1](/voxl2-system-image/#141-voxl2_platform_09targz).

**HW Setup**

<img src="/images/voxl2/m0054-single-tof-m0076.jpg" alt="m0054-single-tof-m0076" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0          | PMD TOF [M0040](/M0040/)             |              | Depth 0      |

**SW Setup**

```bash
voxl2:/$ voxl-configure-cameras 9
```

### CX - Two Time of Flights (ToF) 

Available starting with [Platform Release 0.9, sys img 1.4.1](/voxl2-system-image/#141-voxl2_platform_09targz).

**HW Setup**

Using the [M0076-1](https://www.modalai.com/products/mdk-m0076-1) interposers on `J6` and `J8` as examples:

<img src="/images/voxl2/m0054-dual-tof-m0076.jpg" alt="m0054-dual-tof-m0076" width="1280"/>

Using the [M0084-1](https://www.modalai.com/products/m0084-dual-camera-adapter) dual camera adapter ("y-flex") on `J8` as example:

<img src="/images/voxl2/m0054-dual-tof-m0084.jpg" alt="m0054-dual-tof-m0084" width="1280"/>

| Sensor ID  | Sensor Type         | Notes        | Usage        |
| ---        | ---                 | ---          |---           |
| 0          | PMD TOF [M0040](/M0040/)             |              | Depth 0      |
| 1          | PMD TOF [M0040](/M0040/)             |              | Depth 1      |

**SW Setup**

NA

## Current Limitations

- As a means to allow coexistence of OV7251 sensors on `CCI3`, we are not resetting sensors when they normally should per Qualcomm, to prevent the sensors losing a runtime address swap.  This modification is in the kernel, dmesg shows `MODALAI HACK` where it's happening.
- Sensor ID 1 and Sensor ID 5 share a reset line (gpio100), no specific known defects but an area to watch out.
- Sensor indicies assume all six sensors are in place and will shift when not fully populated.
- All camera AVDD rails in this configuration are left on always (2P8VDC) as they're shared

<img src="/images/voxl2/m0054-camera-config-v0.png" alt="m0054-camera" width="1280"/>
