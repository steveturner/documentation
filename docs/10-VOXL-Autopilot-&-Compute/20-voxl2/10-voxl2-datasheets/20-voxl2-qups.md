---
layout: default
title: VOXL 2 QUPs
parent: VOXL 2 Datasheets
nav_order: 20
permalink: /voxl2-qups/
---

# VOXL 2 QUPs
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

Together, the Qualcomm Universal Peripheral v3 and the TrustZone provide access to various protocols on various hardware interfaces.  Trustzone is a necessary component that requires updates to in order to change protocol/interfaces.

Trusztone images are flashed during the system image installation (`devcfg.mbn`).  The toolchain and source code to build the Trustzone image are unfortunately not open source.

## devcfg

### VOXL 2 - M0054

| QUP            | Protocol | QRB5165 GPIO | Notes                                | Connector/Routing           | Device            | Notes          |
|----------------|----------|--------------|--------------------------------------|-----------------------------|-------------------|----------------|
| QUP0  0x980000 | SPI      | 28-31        | Camera Group 0 SPI                   | J6 - 34,36,38,40, apps proc | `/dev/spidev0.0`  | sys image 1.3+ |
| QUP1  0x984000 | I2C      | 4-5          | Camera Group 1 I2C                   | J7 - 34,36, apps proc       | `/dev/i2c-4`      | sys image 1.5+ |
| QUP2  0x988000 | I2C      | 115-116      | HS B2B I2C2                          | J5 - 8,9                    | `/dev/i2c-0`      |                |
| QUP3  0x98c000 | SPI      | 119-122      | Onboard IMU1, 42688p,                | U7, apps proc               | `/dev/spidev3.0`  | sys image 1.3+ |
| QUP4           | NA       |              |                                      |                             |                   |                |
| QUP5  0x994000 | UART     | 12-15        | Camera Group 2 UART                  | J8 - 34,36,38,40            | `/dev/ttyHS0`     | sys image 1.4+ |
| QUP6           | HS UART  |              |                                      |                             |                   |                |
| QUP7  0x99c000 | HS UART  | 22-23        | B2B 2W UART                          | J3 - 3,5                    | `/dev/ttyHS1`     | sys image 1.3+ |
| QUP8           | NA       |              |                                      |                             |                   |                |
| QUP9  0xa84000 | I2C      | 125-126      | B2B I2C9                             | J3 - 13,15                  | `/dev/i2c-1`      | sys image 1.3+ |
| QUP10 0xa88000 | I2C      | 129-130      | B2B I2C10                            | J3 - 23,25                  | `/dev/i2c-2`      | sys image 1.3+ |
| QUP11 0xa8c000 | SPI      | 60-63        | HS B2B SPI                           | J5 - 53,56                  | `/dev/spidev11.0` | sys image 1.5+ |
| QUP12 0xa90000 | debug    | 34-35        | Debug Console (2W UART, 115200 baud) | J3 - 27,29                  | Not in perf build | sys image 1.3+ |
| QUP13 0xa94000 | UART     |              | HS B2B UART                          | J5 - 95,98                  |                   | sys image 1.5+ |
| QUP14 0x880000 | SPI      | 40-43        | External SPI J10                     | J10, apps proc,             | `/dev/spidev14.0` | sys image 1.3+ |
| QUP15 0x884000 | I2C      | 44-45        | (internal use)                       |                             | `/dev/i2c-3`      |                |
| QUP16          | NA       |              |                                      |                             |                   |                |
| QUP17          | NA       |              |                                      |                             |                   |                |
| QUP18          | NA       |              |                                      |                             |                   |                |
| QUP19 0x894000 | HS UART  | 2-3          | HS B2B 2W UART                       | J5 - 48,49                  | `/dev/ttyHS2`     | sys image 1.3+ |

#### Version 8

- used in system image 1.5+
  - convert qup1 from SPI to I2C to expose apps_proc I2C on camera group

#### Version 7

- used in system image 1.5+
  - enable qup2:  I2C,  for HS B2B J5 pins 8/9, support for M0130 add-on I2C on J8
  - enable qup11: SPI,  for HS B2B J5 pins 53/56, support for M0130 add-on SPI on J8
  - enable qup13: UART, for HS B2B J5 pins 36-39
- [version 7, M0054-VOXL2](todo)(md5sum: 77366973052c03ce3eab57c1e92b490a)

#### Version 6

- used in system image 1.3+
  - enable qup7: 2W UART, for B2B J3 pins 3/5, support for M0125 add-on UART
- [version 6, M0054-VOXL2](todo)(md5sum: 3698389194c899953c4e337a7b48cb97)

### RB5 Flight M0052

Download:

- [M0052-RB5-FLIGHT-](todo) (md5sum: cf613de37db6e7d2bb245f4d17cab79e)

Deltas from M0054 version 6 above:

| QUP            | Protocol  | QRB5165 GPIO   | Notes                                | Connector/Routing    | Device |
|---             |---        |---             |---                                   |---                   |---     |
| QUP5  0x994000 | UART      | 12-15          | Camera Group 2 UART                  | J8 - 34,36,38,40     | `/dev/ttyHS3`  |
| QUP6  0x998000 | HS UART   |                | SOM WiFi                             | internal             | `/dev/ttyHS0`  |
| QUP7  0x99c000 | HS UART   | 22-23          | B2B 2W UART                          | J3 - 3,5             | `/dev/ttyHS4`  |
| QUP13 0xa94000 | UART      | 38-39          | PX4 RC input                         | apps_proc, M0052-J12 | `/dev/ttyHS1`  |
| QUP18 0x890000 | UART      | 58-59          | PX4 GNSS                             | apps_proc, M0052-J10 | `/dev/ttyHS2`  |
| QUP19 0x894000 | HS UART   | 2-3            | HS B2B 2W UART                       | J5 - 48,49           | `/dev/ttyHS5`  |