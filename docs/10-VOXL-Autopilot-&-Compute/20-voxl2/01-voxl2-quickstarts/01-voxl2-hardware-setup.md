---
layout: default
title: VOXL 2 Hardware Setup
parent: VOXL 2 Quickstarts
nav_order: 1
permalink: /voxl2-hardware-setup/
youtubeiId1: lisiTWOa7lM
youtubeiId2: J7iYorzm5rs
---

# VOXL 2 Hardware Setup
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## How to Setup Hardware

### Development Kit Contents

VOXL 2 is available in a variety of [devlopment kits](/voxl2-development-kits/).  Shown here is the `MDK-M0054-1-C11`.

<img src="/images/voxl2/m0054-c11-1-callout.jpg" alt="m0054-c11-1-callout" width="1280"/>

| Callout | Description                                                    | PNs               |
|---      |---                                                             |---                |
|         | Development kit as shown above (includes all below)            | MDK-M0054-1-C11   |
| A       | VOXL 2                                                         | MDK-M0054-1-00    |
| B       | VOXL Power Module v3 and cable                                 | MDK-M0041-1-01    |
| C       | 12V/3A DC wall wart supply with XT60                           | MPS-00005-1       |
| D       | Hires (IMX214) and Tracking (OV7251) image sensors and flexes  | MSU-M0084-1-02    |
| E       | Stereo image sensors and flexes (OV7251) (QTY2 sets shown)     | MSU-M0015-1-02    |

### Hardware Overview

<img src="/images/voxl2/m0054-exploded-v1.3-rev0.jpg" alt="m0054-exploded" width="1280"/>

### First Power On

<br>
**NOTE**:  Please exercise ESD safety precautions and have electronics knowledge when interfacing with the hardware.

Using the image above as reference:

- Connect power cable to VOXL2 (A to B)
- Connect power cable to power module (B to C)
- Connect power module to DC supply and plug in to wall

Three LEDs should illuminate GREEN ([LED details found here](/voxl2-leds)).

{% include youtubePlayer.html id=page.youtubeiId1 %}

The video above demonstrates how to power on VOXL 2, open a bash shell, and run example [open source VOXL SDK programs](https://gitlab.com/voxl-public/voxl-sdk) shipped with the hardware.

### Connecting Image Sensors

Some [development kits](/voxl2-development-kits/) come with image sensors and flexes.  The following video demonstrates how to setup the "C11" configuration which contains 2 stereo pairs, tracking and hires image sensors.

{% include youtubePlayer.html id=page.youtubeiId2 %}

### Modems Options

VOXL 2 offers many modem options to get you onto an IP network.

#### 5G Cellular Connectivity

MCCA-M0090 Modem Carrier Board with a Quectel 5G Modem with USB2/3 ports:

<img src="/images/voxl2/m0054-quickstart-modem-m0090.png" alt="m0054-quickstart-modem-m0090" width="1280"/>

#### 4G Cellular Connectivity

MCCA-M0030 4G/LTE Modem and USB hub:

<img src="/images/voxl2/m0054-quickstart-modem-m0030.png" alt="m0054-quickstart-modem-m0030" width="1280"/>

#### Microhard Connectivity

MCCA-M0048 Microhard Modem Carrier Board with USB hub:

<img src="/images/voxl2/m0054-quickstart-modem-m0048.png" alt="m0054-quickstart-modem-m0048" width="1280"/>

#### Wi-Fi Connectivity

MCCA-M0078 USB Expansion Board with Alfa Network AWUS036ACS 802.11ac AC600 Wi-Fi Wireless Network Adapter:

<img src="/images/voxl2/m0054-quickstart-modem-m0078-wifi.png" alt="m0054-quickstart-modem-m0078-wifi" width="1280"/>

#### Ethernet Connectivity

MCCA-M0078 USB Expansion Board with Apple A1277 Ethernet Adapter:

<img src="/images/voxl2/m0054-quickstart-modem-m0078-eth.png" alt="m0054-quickstart-modem-m0078-eth" width="1280"/>
