---
layout: default
title: VOXL 2 CV Quickstart
parent: VOXL 2 Quickstarts
nav_order: 4
permalink: /voxl2-cv-quickstart/
youtubeId2: J7iYorzm5rs
youtubeId3: 9MC0nBM0BqQ
---

# VOXL 2 Computer Vision Quickstart
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

<img src="/images/voxl2/m0054-cv-quickstart.jpg" alt="m0054-cv-quickstart"/>

[View in fullsize](/images/voxl2/m0054-cv-quickstart.jpg){:target="_blank"}

The VOXL 2 hardware offers:

- QTY 6 full 4 lane MIPI CSI-2 slots
- 6 camera control interfaces (using 4 CCI ports)
- 5 reference voltages per interface

The VOXL 2 software offers:

- [Open source SDK](https://gitlab.com/voxl-public/voxl-sdk)
- Out of the box stack providing raw and processed sensor data through Linux named pipes using the very lightweight Modal Pipe Architecture (MPA)
  - HAL3 based camera server providing frames ready to be consumed
  - IMU server providing high resolution samples
  - QVIO server providing Visual Intertial Odometry
  - DFS server providing depth information
  - many more helper tools

## Software Setup

The following utility can be used to configure the software based on your hardware configuration.

```
voxl-configure-cameras
```

## Configuration File

The configuration file an be inspected and edited if needed here:

```
/etc/modalai/voxl-camera-server.conf
```

## Self Installation

The following video demonstrates how to setup the `C11` development kit.

{% include youtubePlayer.html id=page.youtubeId2 %}

## Cameras using voxl-portal

The following video demonstrates `voxl-portal`:

{% include youtubePlayer.html id=page.youtubeId3 %}