---
layout: default
title: VOXL 2 Networking Guide
parent: VOXL 2 Quickstarts
nav_order: 3
permalink: /voxl2-networking-user-guide/
youtubeId: prZjkMHNfY4
---

# VOXL 2 Networking Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

VOXL 2 keeps the core size and weight low by minimizing component counts and taking techniques like moving modems to add-on cards.  Below are some options for getting VOXL 2 on a network.

## Networking Options

### USB-to-Ethernet Adapters

For desktop development, this is an ideal low-latency option.  

Various add-on options provide USB ports, like the `M0090` or PCIe/5G Modem Carrier board/ provides access to both USB 2.0 and 3.0 hubs via a 4-pin and 10-pin JST connector respectively.

- For USB 2.0, use [4-pin JST to USBA Female Cable](/cable-datasheets/#mcbl-00009)
- For USB 3.0, use [TBD](/cable-datasheets/#mcbl-tbd).

Tested adapters (some were just around the office, and not recommended beyond that endorsement).

- Apple PN# A1277
- Asus PN# TBD

###  Wi-Fi Dongles

See [here](/voxl2-wifidongle-user-guide/).

###  Modems

See [here](/voxl2-modems/).

## Accessing a shell using SSH

Assuming you have an IP connection using one of the options above, you can ssh using user `root` and password `oelinux123`, for example:

```
ssh root@192.168.1.148
...
root@192.168.1.148's password: oelinux123
root@qrb5165-rb5:~#
```

`If you are having issues SSHing into the system as root - login via adb and run the following`
```
echo "PermitRootLogin yes"  >> /etc/ssh/sshd_config
/etc/init.d/ssh restart
```

## Related Video

{% include youtubePlayer.html id=page.youtubeId %}

[Next: VOXL 2 WiFi Dongle Guide](/voxl2-wifidongle-user-guide/){: .btn .btn-green }