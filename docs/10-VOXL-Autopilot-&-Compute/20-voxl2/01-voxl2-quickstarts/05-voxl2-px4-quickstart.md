---
layout: default
title: VOXL 2 PX4 Quickstart
parent: VOXL 2 Quickstarts
nav_order: 5
permalink: /voxl2-px4-quickstart/
youtubeId: aVHBWbwp488
---

# VOXL 2 PX4 Quickstart
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

VOXL 2 ships with PX4 installed.  Communications from VOXL 2/PX4 to ground control stations (like QGroundControl) are over IP, not USB, so a network connection is required.

### HW Block Diagram

<img src="/images/voxl2/m0054-px4-user-guide-overview-v1.png" alt="m0054-px4-user-guide-overview-v1"/>

[View in fullsize](/images/voxl2/m0054-px4-user-guide-overview-v1.png){:target="_blank"}

### SW Block Diagram

<img src="/images/voxl2/voxl2-px4-quickstart-sw-block-diagram.png" alt="voxl2-px4-quickstart-sw-block-diagram.pn"/>

## How to Setup a MAVLink Connection

### IP Conenction

- PX4 ships on VOXL 2 by default and runs on startup
- Obtain an IP connection using a modem (see [here](/voxl2-hardware-setup/) for details)
- Locate the IP address where your Ground Control station is located
- Connect to VOXL 2 using adb or SSH

### voxl-mavlink-server

Enable the `voxl-mavlink-server` by running the following and selecting the factory options:

```bash
voxl-configure-mavlink-server
```

### voxl-vision-px4

Enable `voxl-vision-px4` by running the following, selecting the factory options, and using the IP address of your ground control station when prompted:

```bash
voxl-configure-vision-px4
```

You can edit the `ggc_ip` address in `/etc/modalai/voxl-vision-px4.conf` directly instead of running the wizard in the future.

### Checking PX4 Status

```bash
voxl-inspect-services
```

The following services are required to be running:

- `voxl-mavlink-server`
- `voxl-vision-px4`
- `voxl-px4`

### Related Video

The following video demonstrates how to setup VOXL 2 to connect to QGC using a Wi-Fi dongle:

{% include youtubePlayer.html id=page.youtubeId %}

## How to Connect External Sensors

- GPS/Mag - see [here](/voxl2-guides-onboard-offboard-sensors/)
- RC - see [here](/voxl2-rc-configs/)

## How to Connect ESCs

- see [here](/voxl2-esc-configs/)

## More Information

- [VOXL 2 PX4 User Guide](/voxl-px4-user-guide/)
- [VOXL 2 PX4 Developer Guide](/voxl-px4-developer-guide/)
- [VOXL 2 PX4 Build Guide](/voxl-px4-build-guide/)
