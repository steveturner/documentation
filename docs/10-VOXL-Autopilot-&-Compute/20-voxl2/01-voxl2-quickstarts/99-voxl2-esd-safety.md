---
layout: default
title: VOXL 2 ESD Safety
parent: VOXL 2 Quickstarts
nav_order: 99
permalink: /voxl2-esd-safety/
---

# VOXL 2 ESD Safety
{: .no_toc }


---

Please exercise ESD safety precautions and have electronics knowledge when interfacing with the hardware.

<img src="/images/voxl2/m0054-esd-warning.png" alt="m0054-esd-warning"/>
