---
layout: default
title: VOXL 2 VS Code User Guide
parent: VOXL 2 User Guides
nav_order: 23
permalink:  /voxl2-vs-code-user-guide/
youtubeId: SO_OG64fK30
---

# VOXL 2 VS Code
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---


## Overview

Microsoft's VS Code is a free and open source IDE.  In the video below, you'll learn how to use VS Code to remotely browse the VOXL2 file system and build a package on target.

## Video

{% include youtubePlayer.html id=page.youtubeId %}

[Next: VOXL 2 Kernel Build Guide](/voxl2-kernel-build-guide/){: .btn .btn-green }