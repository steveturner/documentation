---
layout: default
title: VOXL 2 PX4 to QGC Guide
parent: VOXL 2 User Guides
nav_order: 10
permalink:  /voxl2-px4-to-qgc-guide/
youtubeId: aVHBWbwp488
---

# VOXL 2 PX4 to QGC Guide
{: .no_toc }

---

## Summary

- Connection to QGC over the USBC interface is not yet supported
- An IP connection (e.g. Wi-Fi, Microhard, WWAN) is required to make a connection to QGC from PX4

The following video walks you through setting up the connection using WiFi

{% include youtubePlayer.html id=page.youtubeId %}

A quickstart guide walking through these same setups can be found [here](/voxl2-px4-quickstart/).
