---
layout: default
title: VOXL 2 Modems
parent: VOXL 2 User Guides
nav_order: 8
permalink:  /voxl2-modems/
---

# VOXL 2 Modem User Guides
{: .no_toc }

---

## Summary

Overview of VOXL 2 modem user's guides.

## Modem Compatibility and User Guides

| Air Interface | User Guide                                    | Purchase                                                                  |
|---------------|-----------------------------------------------|---------------------------------------------------------------------------|
| WiFi          | [User's Guide](/voxl2-wifidongle-user-guide/) | [Purchase](https://www.modalai.com/products/wifi-usb-cable-add-on)        |
| Microhard     | [User's Guide](/microhard-add-on-manual/)     | [Purchase](https://www.modalai.com/products/voxl-microhard-modem-usb-hub) |
| 4G LTE        | [User's Guide](/lte-v2-modem-user-guide/)     | [Purchase](https://www.modalai.com/products/voxl-lte)                     |
| 5G            | [User's Guide](/5G-Modem-user-guide/)         | [Purchase](https://www.modalai.com/products/m0090)                        |
| Doodle Labs   | [User's Guide](/doodle-labs-user-guide/)      | Coming in the Future                                                      |
