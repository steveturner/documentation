---
layout: default
title: VOXL 2 External Flight Controller
parent: VOXL 2 User Guides
nav_order: 15
permalink:  /voxl2-external-flight-controller/
---

# VOXL 2 Connecting an External Flight Controller
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Summary

Although VOXL 2 has the capability to run PX4 in a real time DSP, some use cases call for an external flight controller.

<img src="/images/voxl2/m0054-ext-fc-real.jpg" alt="m0054-ext-fc-real"/>

## UART Communication Options

You can communicate between a flight controller TELEM port and a VOXL 2 UART port.  For example, Flight Core has a default telemetry port on J5 with a MavLINK instance running at 57600 baud.

Starting in System Image 1.3, there are [3 applications processor UARTs exposed](/voxl2-linux-user-guide/#uarts).  Additionally, there are 3 DSP UARTs used by PX4, that will be made available in future softwware releases via and API or similar.

| UART            | Connector               | Add-On / Adapter |
|------           |-------------------------|----------------- |
| `/dev/ttyHS0`   | [J8](/voxl2-connectors/), camera group 2      | [M0076](https://www.modalai.com/products/mdk-m0076-1) with hand soldering to test points |
| `/dev/ttyHS1`   | [J3](/voxl2-connectors/), Board to Board      | [M0125](/voxl2-usb3-uart-add-on/) / [Purchase Here](https://www.modalai.com/products/usb-3-uart-expansion-adapter?variant=40740708876339) |
| `/dev/ttyHS2`   | [J5](/voxl2-connectors/), HS Board to Board   | [M0090](/5G-Modem-datasheet/) / [Purchase Here](https://www.modalai.com/products/m0090?variant=40594936463411) |


## Hardware Setup

### Using USB/UART Addon Board (M0125)

In the following example, we'll add the USB3/UART Add-On Board (M0125) to VOXL 2 (M0054) which gives us a 4 pin JST GH connector with a UART availabe at `/dev/ttyHS1` from the applications processor.

Using `MCBL-00067-1`, connect from M0125's J3 to Flight Core's (v1/v2) (M0018/M0087) J5, TELEM1/TELEM2 (respectively).

<img src="/images/voxl2/m0054-ext-fc-m0125.png" alt="m0054-ext-fc-m0125.png"/>

| M0125 J3  | M0018 J5 |
|------     |---       |
| 1         |          |
| 2 RX      | 2 TX     |
| 3 TX      | 3 RX     |
| 4 GND     | 6 GND    |

### Using M.2 5G Modem Addon Board (M0090)

The VOXL2 (`M0054`) can have the M.2 5G Modem Board Addon attached, which exposes a UART on `M0090 - J9` that is accessible in VOXL2's application processor as `/dev/ttyHS2`.

Using `MCBL-00066-1`, connect from M0090's J9 to Flight Core's (v1/v2) (M0018/M0087) J5, TELEM1/TELEM2 (respectively).

<img src="/images/voxl2/m0054-ext-fc-m0090.png" alt="m0054-ext-fc-m0090.png"/>

| M0090 J9  | M0018 J5 |
|------     |---       |
| 1         |          |
| 2 TX      | 3 RX     |
| 3 RX      | 2 TX     |
| 4         |          |
| 5         |          |
| 6         | 6 GND    |

### Using J8 Camera Connector

You will need to use a level shifter likely, as the logic is 1.8V here.  VOXL2's camera group 3 connector can be used with M0076 interposer which has test points you can solder to.

<img src="/images/voxl2/m0054-ext-fc-m0076.jpg" alt="m0054-ext-fc-m0076.jpg"/>

- UART_TX - TP7
- UART_RX - TP8
- VREF 1P8 - TP1
- DGND - TP11

## VOXL SDK Software Setup

### Component Overview

The following image captures the main components in play when using VOXL2 with a USB/UART Addon Board and a Flight Core.

<img src="/images/voxl2/m0054-ext-fc-sw-3.png" alt="m0054-ext-fc-sw-3.png"/>

### VOXL2 SW Setup

#### Disable PX4 Running on VOXL2

If you aren't using PX4 on VOXL 2, you should likely disable it from running on bootup.

```
adb shell
systemctl disable voxl-px4
```

#### Setup voxl-mavlink-server.conf

voxl-mavlink-server ships in the VOXL SDK, if needed, run `voxl-configure-mavlink-server` to setup.  If already setup or to edit manually, locate `voxl-mavlink-server.conf`.  For the `px4_uart_bus` field, these are current options:

- `/dev/ttyHS0` --> "px4_uart_bus": 0
- `/dev/ttyHS1` --> "px4_uart_bus": 1
- `/dev/ttyHS2` --> "px4_uart_bus": 2

For `px4_uart_baudrate`, configure this the same as the MAVLink instance on Flight Core.  In this example, we are using `921600`.

For `external_fc`, set to `true`.

For the PX4 side, on Flight Core v1 (M0018), check the `SER_TEL1_BAUD` setting if using the hardware setup above (on Flight Core v2 (M0087), check `SER_TEL2_BAUD`).

Here's the setup to use if following allong and using `M0125` like the picture at the top of this guide.

```
/**
 * voxl-mavlink-server Configuration File
 * UART fields are for APQ8096 VOXL1 or QRB5165 VOXL2 with external fc
 * UDP fields are for PX4 on SDSP on QRB5165 only
 * External FC field is for QRB5165 only
 *
 */
{
	"px4_uart_bus":	1,
	"px4_uart_baudrate":	921600,
	"udp_port_to_px4":	14556,
	"udp_port_from_px4":	14557,
	"external_fc":	true
}
```

#### Start voxl-mavlink-server

While setting things up, let's manually start `voxl-mavlink-server`.

```
adb shell
voxl2:/$ voxl-mavlink-server
loading our own config file
=================================================================
Parameters as loaded from config file:
px4_uart_bus:               1
px4_uart_baudrate:          921600
udp_port_to_px4:            14556
udp_port_from_px4:          14557
external_fc:                1
=================================================================
Configuring for external flight controller
Successfully opened /dev/ttyHS1
starting receive thread
Init complete, entering main loop
```

To run this on startup, you can enable using `systemctl enable voxl-mavlink-server`.

### PX4 SW Setup

You need to connect over the USB interface to configure the external flight controller, in this guide's case the Flight Core v1.

- Connect to QGroundControl over USB and open the parameters.
  - On Flight Core (M0018), J5 is mapped to`TELEM1` in PX4 (/dev/ttyHS6).  For this example to work out of the box, make sure to set  `SER_TEL1_BAUD` to the default baud rate of  **921600** if you've changed it.
  - On Flight Core (M0087), J5 is mapped to`TELEM2` in PX4 (/dev/ttyHS4), so use `SER_TEL2_BAUD` instead.

Check the status of MAVLink instances by opening the MAVLink Console in QGC and running `mavlink status`:

On Flight Core v1, you should see something like this if using `921600` baud:

```bash
instance #1:
    mavlink chan: #1
    type:        GENERIC LINK OR RADIO
    flow control: ON
    rates:
      tx: 81.8 B/s
      txerr: 0.0 B/s
      tx rate mult: 0.682
      tx rate max: 1200 B/s
      rx: 20.9 B/s
      rx loss: 0.0%
      received from sysid: 0 compid: 197: 43, lost: 0, last 343 ms ago
    FTP enabled: YES, TX enabled: YES
    mode: Normal
    MAVLink version: 2
    transport protocol: serial (/dev/ttyS6 @921600)
```

Above is showing you that the Flight Core is communicating with VOXL2 successfully, note `tx: 81.8 B/s` and `rx: 20.9 B/s` rates indicate bidirectional traffic.

## pymavlink Software Based Example

{: .alert .warning-alert}
**WARNING:** This clashes with the example above, so ensure to disable the `voxl-mavlink-server` if you configured the VOXL SDK.

The `SER_TEL1_BAUD`/`SER_TEL1_BAUD` baud rate should be set to `57600` for thi example (for Flight Core v1/v2 respectively).

<img src="/images/voxl2/m0054-ext-fc-py.png" alt="m0054-ext-fc-py"/>

First, connect VOXL 2 to the internet so we can use `apt` to install `pymavlink`.  You can connect an ethernet or WiFi dongle to the USB3 port.

Conect to the VOXL 2 over ADB:

```
adb shell
```

Now on target:

```
apt update
sudo apt-get install python3-pip
sudo apt-get install libxml2-dev libxslt-dev python-dev
sudo pip3 install pymavlink pyserial
```

Get the `mavlink_shell.py` tool from PX4:

```
cd /home
wget https://raw.githubusercontent.com/PX4/PX4-Autopilot/main/Tools/mavlink_shell.py
```

Now, run the program and interact with the Flight Core.

```
python3 mavlink_shell.py /dev/ttyHS1
Connecting to MAVLINK...

NuttShell (NSH)

nsh> ver all

HW arch: MODALAI_FC_V1
HW type: V106
HW version: 0x00000000
HW revision: 0x00000006
FW git-hash: 7008425b466eba94e35304e502454208d578ba56
FW version: 1.11.3 0 (17498880)
FW git-branch: fcio-v2
OS: NuttX
OS version: Release 8.2.0 (134349055)
OS git-hash: ec20f2e6c5cc35b2b9bbe942dea55eabb81297b6
Build datetime: May 10 2022 18:38:31
Build uri: localhost
Toolchain: GNU GCC, 9.3.1 20200408 (release)
PX4GUID: 0002000000003930353534385109003a002e
MCU: STM32F76xxx, rev. Z
```
