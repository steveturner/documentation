---
layout: default
title: VOXL 2 Datasheets
nav_order: 10
parent: VOXL 2
has_children: true
permalink: /voxl2-datasheets/
---


# VOXL 2 Datasheets
{: .no_toc }

<img src="/images/voxl2/m0054-exploded-v1.3-rev0.jpg" alt="m0054-exploded" width="1280"/>

# Mechanical Drawings

## 2D Drawings

![voxl2-imu-locations](../../images/voxl2/m0054_voxl2_imu_locations_with_axis.png)

## 3D Drawings

[3D STEP File](https://storage.googleapis.com/modalai_public/modal_drawings/M0054_VOXL2_PVT_SIP_REVA.step)
