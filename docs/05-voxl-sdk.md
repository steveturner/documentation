---
layout: default
title: VOXL SDK
nav_order: 05
has_children: true
permalink: /voxl-sdk/
---

# VOXL SDK
{: .no_toc }

## Overview

VOXL SDK (Software Development Kit) consists of the open source [core libraries](/core-libs/), [services](/mpa-services/), [tools](/inspect-tools/), [utilities](/sdk-utilities/), and [build environments](/build-environments/) that ModalAI provide to accelerate the use and development of [VOXL compute boards](/voxl-computers/) and accessories.

ModalAI is primarily a manufacturer of small [compute boards](/voxl-computers/) designed to be integrated into final products by other OEMs. This section describes our Software Development Kit designed to help accelerate your own software development on [VOXL hardware](/voxl-computers/).

VOXL SDK runs on [VOXL](/voxl/), [VOXL 2](/voxl-2/), and [RB5 Flight](/qualcommflightrb5/)!

The build environment details for compiling code for the CPU, GPU, DSPs, and NPU can be found at the [build-environments](/build-environments/) page.

The source code for projects within VOXL SDK can be found at [https://gitlab.com/voxl-public](https://gitlab.com/voxl-public) alongside build instructions. This section of the manual serves to provide a description of the use and functionality of the software.


![voxl-sdk-map](/images/voxl-sdk/voxl-sdk-mpa.png)


Please continue reading through the following sections to learn more:
